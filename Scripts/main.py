﻿import new_project
import open_project
import about_program
from reports import report_generation


def main():
    while True:
        print('1. Создать проект', '2. Открыть проект', '3. Справка', '4. Сформировать отчет', '5. Выход', sep = '\n')

        try: choise = int(input('>> '))
        except ValueError: continue

        if choise == 1:     new_project.new()
        elif choise == 2:
            open_project.open('')
        elif choise == 3:   about_program.about()
        elif choise == 4:   report_generation.general()
        elif choise == 5:	break


if __name__ == '__main__':
    main()
