from .report import main
import os
import subprocess
import configparser


def general():

    print('=== Работа с отчетами ===')
    while True:
        print('\n1. Ввести информацию и сгенерировать отчёты', '2. Выход', sep='\n')
        # print('\n1. Ввести имя проекта и сгенерировать файл конфигураций', '2. Сгенерировать отчет согласно файла конфигураций', '3. Выход', sep = '\n')
        try:
            choise = int(input('>> '))
        except ValueError:
            continue

        if choise == 1:
            prepare()
        elif choise == 2:
            return

        # if choise == 1:
        #     prepare()
        # elif choise == 2:
        #     generate()
        # elif choise == 3:
        #     return


def generate():
    
    path = os.getcwd()
    print(path)
    to_jar = path + '\\reports\\RepGen.jar'
    print(to_jar)
    to_ref = path[0:path.rfind('\\')] + '\\Projects\\reference.txt'
    print(to_ref)

    # with open('valo.txt', 'w') as outf:
    #     outf.write('java -jar ' + to_jar + ' ' + to_ref)
    # subprocess.call('java -jar ' + to_jar + ' ' + to_ref)


def prepare():

    print('\n== Генерация конфигурационного файла ==\n')
    print("Введите название отчета или 'b' для возврата': \n")
    
    try:
        name = input('>> ')
    except ValueError:
        print('Некорректное имя')

    if name =='b':
        return

    print("Перечислите через ';' проекты, по которым необходимо сгенерировать отчёты.")
    print('Формат ввода: имя проекта, децимальный номер, уровень анализа:\n')
    projects = input().split(';')
    main({'name': name, 'projects': projects})

# Старый код
    # Открытие файла конфигураций и запись по образцу
#     path = os.getcwd()
#     path = path[0:path.rfind('\\')] + '\\Projects\\'
#     reference = open (path + 'reference.txt', 'w', encoding='utf-8')
#     reference.write(name)

#     level = 0
#     for root, dirs, files in os.walk(path):
#         for fname in files:
#             # База должна носить то же имя, что и проект
#             # Другие базы не должны попадать в конфигурационный файл
#             folders = root.split('\\')
#             # Прыгаем на два уровня вверх
#             folder = folders[len(folders)-2]
#             if os.path.splitext(fname)[1] == '.conf' and fname[0:fname.rfind('.')] == folder:
#                 cfg = configparser.ConfigParser()
#                 cfg.read(os.path.join(root,fname))
#                 level = cfg.getint('common','level')
#             if fname.endswith('.sdb') and fname[0:fname.rfind('.')] == folder:
#                 ab = os.path.abspath(os.path.join(root, fname))
#                 reference.write('\n'+ ab + ',номер СПО' + ',' + str(level))
#     print('\n Данные по всем проектам занесены в файл конфигураций. \
# \nВ папке Smile\\src\\Projects отредактируйте файл reference.txt, удалив проекты, \
# \nдля которых не нужно формировать отчет. После запятой введите децимальный номер \
# СПО и сохраните изменения. После этого выберите опцию 2; выходной отчет \
# \nбудет располагаться в папке Smile\\src\\Reports. \n')

#     reference.close()

# for testing
if __name__ == '__main__':
    general()