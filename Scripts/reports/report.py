'''
author ФедощенкоЕС
port to python БондаревВГ
'''
import pathlib
import sys
import shutil
import sqlite3
from math import ceil
from collections import OrderedDict
from mako.lookup import TemplateLookup


# Главная функция, принимает путь до файла reference.txt
# (что-то вроде r'c:\Smile\Projects\reference.txt')
def main(arg=None):
    if arg is None:
        print('Wrong argument, no path to reference.txt')
        return
    
    # smilePath for testing
    smilePath = pathlib.Path.cwd().parents[1]

    # smilePath = pathlib.Path.cwd().parent
    reportsPath = smilePath / 'Reports'
    templates = smilePath / 'Scripts' / 'reports' / 'templates'
    spos = list()

    if not reportsPath.exists():
        reportsPath.mkdir(parents=True)

    projTitle = arg['name'].strip()

    bad_projects = list()
    for project in arg['projects']:
        project = project.split(',')
        name = project[0].strip()
        title = project[1].strip()
        try:
            level = int(project[2].strip())
        except ValueError as error:
            bad_projects.append({'name': name, 'dnumber': title, 'level': project[2].strip()})
            print(error)
            continue
        sdbPath = smilePath / 'Projects' / name / 'Databases' / (name + '.sdb')
        if not sdbPath.exists() or not sdbPath.is_file():
            print("Wrong path! The database doesn't exist. Please, check the path %s." % sdbPath)
            bad_projects.append({'name': name, 'dnumber': title, 'level': level})
            continue
        if not 1 < level < 5:
            print("Wrong analysis level! Please, check: project name: %s, level: %d: " % (name, level))
            bad_projects.append({'name': name, 'dnumber': title, 'level': level})
            continue
        prepPath = reportsPath / projTitle / 'udf' / name
        print(prepPath)
        if not prepPath.exists():
            prepPath.mkdir(parents=True)
        doALot(sdbPath, prepPath, name, title, templates, level)
        e = dict()
        e['name'] = name
        e['title'] = title
        spos.append(e)

    if spos:
        print("\nCreating router file...")
        createRouterHTML(spos, projTitle, reportsPath, templates)
        print("Copying image...")
        dst = reportsPath / projTitle / 'udf' / 'ready.jpg'
        src = templates / 'ready.jpg'
        copyFile(str(src), str(dst))
    if bad_projects:
        print("\nSome errors occured during bilding the report.")
        print("Please, check correctness following information:")
        for project in bad_projects:
            for key, value in project.items():
                print(key, ': ', value, sep='', end=' ')
            print()
    else:
        print("\nReports creation is finished successfully.")



# Функция doALot
# Бывший main для единичной базы.
# Получает запросы для отчетов и передает функции doMagic, 
# которая формирует конечные атомарные отчеты.
def doALot(dbpath, pPath, title, rusTitle, templates, level):
    print('Processing database ' + str(dbpath) + ' at level: ' + str(level))
    queries = generateQueries(level)
    dynQueries = generateDynQueries(level)
    names = createNames()

    dbCon = sqlite3.connect(str(dbpath))

    # сгенерировать отчеты по статике
    print('\nStatics:')
    for key, value in queries.items():
        print('Generating ' + key + ' report...')
        doMagic(dbCon, value, key, names, title, pPath, templates, level)
        print('Report ' + key + ' OK.')
    print('\nDynamincs: ')
    # сгенерировать отчеты по динамике
    for key, value in dynQueries.items():
        print('Generating ' + key + ' report...')
        doMagic(dbCon, value, key, names, title, pPath, templates, level)
        print('Report ' + key + ' OK.')
    # сгенерировать файл root.html
    # заслать данные в модельку
    reports = list()
    orignames = createNames()
    for key, value in orignames.items():
        if queries.get(key) is not None:
            reports.append({'title': value, 'path': names[key]})

    # Динамические отчеты
    dyn_bin_report = {'title': orignames['dyn_bin'], 'path': names['dyn_bin']}
    dyn_fun_report = {'title': orignames['dyn_fun'], 'path': names['dyn_fun']}

    rootData = dict()
    rootData['dyn_bin_report'] = dyn_bin_report
    rootData['dyn_fun_report'] = dyn_fun_report
    rootData['entitle'] = title
    rootData['title'] = rusTitle
    rootData['reports'] = reports
    rootData['level'] = level
    if level < 4:
        getCoverage(level, rootData, dbCon)
    createRootHTML(pPath, rootData, templates)
    dbCon.close()


# Функция doMagic
# Отвечает за создание одного отчета из одной базы данных
def doMagic(dbCon, query, entitle, names, spo, path, templates, level):
    # Словарь уровней группировки данных; необходим для корректного 
    # создания временного шаблона rtemp в функции createTemp и
    # корректной модели данных
    grLvl = dict()

    # Статика
    grLvl['binaries']        = 1 # каталог
    grLvl['sources']         = 1 # каталог
    grLvl['bin-source-link'] = 1 # исполняемый файл
    grLvl['bin-without-src'] = 0
    grLvl['exc-sources']     = 1 # каталог
    grLvl['functions']       = 1 # файл исходного кода
    grLvl['exc-functions']   = 1
    grLvl['gVar']            = 1 # файл исходного кода
    grLvl['gVar-fun-link']   = 1 # функция
    grLvl['fun-fun-link']    = 1 # функция-родитель
    grLvl['lin']             = 2 # файл исходного кода => функция
    grLvl['lin-lin-link']    = 3 # файл => функция => родительский участок

    # Динамика
    grLvl['dyn_bin'] = 1 # каталог
    grLvl['dyn_fun'] = 1 # src-файл

    cursor = dbCon.cursor()
    # Общее число строк, от него зависит разбиение на страницы
    try:
        cursor.execute('select count(*) from ( ' + query + ' )')
        total = cursor.fetchone()[0]
        print('Fetched ' + str(total) + ' rows from ' + entitle)
        cursor.execute(query)

        # Создание шаблона для нового отчета с определенным параметром grLvl       
        try:
            makeTemp(cursor, grLvl.get(entitle), templates, entitle, spo, level)
        except Exception as error:
            print('Could not generate template. %s' % error)

        title = names.get(entitle)
        if total > 100:
            names[entitle] = title + "-Страница 1"
            for i in range(1, ceil(total / 100) + 1):
                try:
                    goHTML(createDM(cursor, title, total, i), path / (title + "-Страница " + str(i) + ".html"), templates)
                except Exception as error:
                    print('Multiple goHTML exception')
                    print(error.args)
                    dbCon.close()
                    sys.exit(2)
        else:
            try:
                goHTML(createDM(cursor, title, total, 1), path / (title + ".html"), templates)
            except Exception as error:
                print('Single goHTML exception')
                print(error.args)
                dbCon.close()
                sys.exit(2)
    except Exception as error:
        print('doMagic exception')
        print(error.args)


# Функция makeTemp
# Создание начинки для конкретного отчета
def makeTemp(cursor, grLvl, templates, entitle, spo, level):
    with (templates / 'rtemp').open('w', encoding='utf-8') as template:
        # Русские названия для полей
        map = dict()
        map['id']           = 'id'
        map['id_src']       = 'id src'
        map['id_bin']       = 'id bin'
        map['fullname']     = 'Полное имя'
        map['is_useful']    = 'Используется'
        map['is_marked']    = 'Помечен'
        map['len']          = 'Размер'
        map['crc']          = 'КС'
        map['num']          = 'ЛУ'
        map['glob']         = 'id'
        map['name']         = 'Имя'
        map['source']       = 'Файл исходного текста'
        map['parameters']   = 'Параметры'
        map['ret_type']     = 'Тип'
        map['lines']        = 'Длина'
        map['offset']       = 'Начало'
        map['def_file']     = 'Файл'
        map['type']         = 'Тип'
        map['called']       = 'Ветвь-2'
        map['calling']      = 'Ветвь-1'
        map['percent']      = 'Покрытие'
        map['barcontainer'] = ' '
        map['chck']         = 'Срабатывание'

        """ Новая генерация шаблона rtemp """
        template.write("<%\n")
        template.write("    x = 1\n")
        template.write("    if data.get('pagination') != None:\n")
        template.write("        x = 100 * (data['pagination']['current'] - 1) + 1\n")
        template.write("    param = 0\n")
        template.write("    param2 = 0\n")
        template.write("    param3 = 0\n")
        template.write("%>\\\n")

        template.write("""<%def name="color(percent, type='router')">\n""")
        template.write("    % if percent < 25:\n")
        template.write("        % if type == 'router':\n")
        template.write("            <span style='width: ${percent}%; background-color: ED6732;'></span>\n")
        template.write("        % else:\n")
        template.write("            <span style='width: ${percent}%; background-color: 29A63A;'></span>\n")
        template.write("        % endif\n")
        template.write("    % elif percent < 50:\n")
        template.write("        <span style='width: ${percent}%; background-color: E6CB32;'></span>\n")
        template.write("    % elif percent < 75:\n")
        template.write("        <span style='width: ${percent}%; background-color: AEDB4B;'></span>\n")
        template.write("    % else:\n")
        template.write("        % if type == 'router':\n")
        template.write("            <span style='width: ${percent}%; background-color: 29A63A;'></span>\n")
        template.write("        % else:\n")
        template.write("            <span style='width: ${percent}%; background-color: ED6732;'></span>\n")
        template.write("        % endif\n")
        template.write("    % endif\n")
        template.write("</%def>\\\n")

        template.write('<tr class=heady>\n')
        template.write('<th class=num> No. </td>\n')

        colCount = len(cursor.description)    
        for i in range(colCount - 4 * grLvl):
            columnName = cursor.description[i][0]
            if columnName == 'pad' or columnName == 'is_useful':
                template.write('<th>&nbsp</td>\n')
            elif columnName == 'udb_id':
                template.write('<td></td>\n')
            else:
                template.write('<th class=' + columnName + '>' + map.get(columnName, "") + '</td>\n')
        template.write('</tr>\n')

        template.write("% for d in data['common']:\n")
        template.write("    % if d.get('param_value') != None:\n")
        template.write("        % if param != d['param_value']:\n")
        template.write("        <% param = d['param_value'] %>\n")
        template.write("<tr>\n")
        template.write("    <td colspan=" + str(colCount - 4 * grLvl + 1) + ">\n")
        template.write("        <table celpadding=0 cellspacing=0>\n")
        template.write("            <tr class=group-1>\n")
        template.write("                <td class=param_name>${d['param_name']}</td>\n")
        template.write("                <td class=param_value>${param}</td>\n")
        template.write("% if d.get('param_count') != None:\n")
        template.write("                <td class=param_count>n = ${d['param_count']}</td>\n")
        template.write("% endif\n")
        template.write("% if d.get('param_id') != None:\n")
        template.write("                <td class=param_id> id = ${d['param_id']} </td>\n")
        template.write("% endif\n")
        template.write("                <td class=pad><p></td>\n")
        template.write("            </tr>\n")
        template.write("        </table>\n")
        template.write("    </td>\n")
        template.write("</tr>\n")
        template.write("        % endif\n")
        template.write("    % endif\n")

        # Второй уровень группировки
        template.write("    % if d.get('param2_value') != None:\n")
        template.write("        % if param2 != d['param2_value']:\n")
        template.write("        <% param2 = d['param2_value'] %>\n")
        template.write("<tr>\n")
        template.write("    <td colspan=" + str(colCount - 4 * grLvl + 1) + ">\n")
        template.write("        <table celpadding=0 cellspacing=0>\n")
        template.write("            <tr class=group-2>\n")
        template.write("                <td class=pad><p></td>\n")
        template.write("                <td>${d['param2_name']}</td>\n")

        if entitle == 'lin' or entitle == 'lin-lin-link':
            if spo.find('ESD') != -1:
                template.write("% if d.get('udb_id') != None:\n")
                template.write("""                <td><a href='../../../_rc/ESD/${d['udb_id']}.png'>\
${d['param2_value']}</a></td>\n""")
                template.write("% else:\n")
                template.write("                <td>${d['param2_value']}</td>\n")
                template.write("% endif\n")
            else:
                template.write("% if d.get('udb_id') != None:\n")
                template.write("                <td><a href='../../../_rc/" + spo +
                               "/${d['udb_id']}.png'>${d['param2_value']}</a></td>\n")
                template.write("% else:\n")
                template.write("                <td>${d['param2_value']}</td>\n")
                template.write("% endif\n")
        else:
            template.write("                <td>${d['param2_value']}</td>\n")
        template.write("% if d.get('param2_count') != None:\n")
        template.write("                <td>n = ${d['param2_count']}</td>\n")
        template.write("% endif\n")
        template.write("% if d.get('param2_id') != None:\n")
        template.write("                <td></td><td>id = ${d['param2_id']}</td>\n")
        template.write("% endif\n")
        template.write("                <td class=pad><p></td>\n")
        template.write("            </tr>\n")
        template.write("        </table>\n")
        template.write("    </td>\n")
        template.write("</tr>\n")
        template.write("            % if d.get('param3_value') != None:\n")
        template.write("                % if param3 == d['param3_value']:\n")
        template.write("            <tr>\n")
        template.write("                <td></td><td><b>${d['param3_value']}</b></td>\n")
        template.write("                <td></td><td><b>Всего связей: ${d['param3_count']}</b></td>\n")
        template.write("            </tr>\n")
        template.write("                % endif\n")
        template.write("            % endif\n")
        template.write("        % endif\n")
        template.write("    % endif\n")

        # Третий уровень группировки
        template.write("    % if d.get('param3_value') != None:\n")
        template.write("        % if param3 != d['param3_value']:\n")
        template.write("        <% param3 = d['param3_value']%>\n")
        template.write("<tr>\n")
        template.write("    <td></td><td><b>${d['param3_value']}</b></td>\n")
        template.write("    <td></td><td><b>Всего связей: ${d['param3_count']}</b></td>\n")
        template.write("</tr>\n")
        template.write("        % endif\n")
        template.write("    % endif\n")

        template.write("% if x % 2 == 0:\n")
        template.write("<tr class=even>\n")
        template.write("% else:\n")
        template.write("<tr class=odd>\n")
        template.write("% endif\n")

        # Нумерация
        template.write("    <td> ${x} </td>\n")
        template.write("<% x += 1 %>\\\n")
        # Заполнение шаблона именами полей модели данных
        for i in range(colCount - 4 * grLvl):
            columnName = cursor.description[i][0]
            
            if columnName == "pad":
                template.write("    <th>&nbsp</td>\n")
            elif columnName == "name" and (entitle == "functions" or entitle == "dyn_fun") and level == 2:
                if spo.find("ESD") != -1:
                    template.write("    <td><a href='../../../_rc/ESD/${d['udb_id']}.png'>${d['name']}</a></td>\n")
                else:
                    template.write("    <td><a href='../../../_rc/" + spo
                        + "/${d['udb_id']}.png'> ${d['name']}</a></td>\n")
            elif columnName == "udb_id":
                template.write("    <td></td>\n")
            elif columnName == "chck":
                template.write("    <td>\n")
                template.write("""        <div class = 'chuck'><span style='width: 100%; background-color: \
${d['chck']};'></span></div>\n""")
                template.write("    </td>\n")
            elif columnName == "percent":
                template.write("    <td class = percent>${d['percent']}%</td>\n")
            elif columnName == "barcontainer":
                template.write("    <td class = 'barcontainer'>\n")
                template.write("        <div class='bar'>\\\n")
                template.write("${color(d['percent'])}\\\n")
                template.write("        </div>\n")
                template.write("    </td>\n")
            elif columnName == 'parameters':
                template.write("% if d['parameters'] == '':\n")
                template.write("    <td>None</td>\n")
                template.write("% else:\n")
                template.write("    <td>${d['parameters']}</td>\n")
                template.write("% endif\n")
            else:
                template.write("    <td>${d['" + columnName + "']}</td>\n")
        template.write("</tr>\n")

        template.write("% endfor\n")


# Функция createDM
def createDM(cursor, title, total, current):
    data = dict()
    common = list()
            
    # TODO:  зараза съедает на итерациях одну запись при путешествии по страницам
    # upd уже не съедает, у кого-то кривые руки =\

    data['title'] = title
    
    if total == 0:
        data['none'] = "В ходе анализа искомые элементы не обнаружены."
        return data

    queryResult = cursor.fetchmany(100)
    querySize = len(queryResult)
    currentRow = 0
    columnCount = len(cursor.description)
    for currentRow in range(querySize):
        e = dict()
        for i in range(columnCount):
            columnName = cursor.description[i][0]
            if str(queryResult[currentRow][i]) == '':
                e[columnName] = ''
            elif columnName == 'percent':
                e[columnName] = int(queryResult[currentRow][i])
            elif columnName == "chck":
                if int(queryResult[currentRow][i]) < 2:
                    e[columnName] = "#ED6732"
                else:
                    e[columnName] = "#29A63A"
            else:
                e[columnName] = str(queryResult[currentRow][i])
        common.append(e)
        currentRow += 1

    # Группа свойств для разделения на страницы
    if total // 100 > 0:
        pagination = {'current': current, 'last': ceil(total / 100)}
        data['pagination'] = pagination
    # Собираем все свойства воедино
    data['common'] = common
    return data


# Функция goHTML
# Формирует конечные html-страницы из шаблона templ (который включает в себя шаблоны nav и rtemp)
# и данных, полученных от функции createDM
def goHTML(args, fname, templates):
    with fname.open('bw') as outf:
        template = TemplateLookup(directories=str(templates), input_encoding='utf-8', output_encoding='utf-8').get_template('templ')
        outf.write(template.render_unicode(data=args).encode('utf-8', 'replace'))


# Функция createRootHTML
# Создает html-страницы root*.html
def createRootHTML(pPath, args, templates):
    list_names = list()
    list_names.append({'root_name': 'root_sample', 'router_name': 'router_sample',
                       'app_name': 'Приложения к акту отбора образцов'})
    list_names.append({'root_name': 'root_id', 'router_name': 'router_id',
                       'app_name': 'Приложения к протоколу идентификации'})
    list_names.append({'root_name': 'root', 'router_name': 'router_udf',
                       'app_name': 'Приложения к протоколу НДВ'})
    for names in list_names:
        with (pPath / (names['root_name'] + '.html')).open('bw') as outf:
            args['names'] = names
            template = TemplateLookup(directories=str(templates), input_encoding='utf-8', output_encoding='utf-8').get_template('root')
            outf.write(template.render_unicode(data=args).encode('utf-8', 'replace'))


# Функция createRouterHTML
# Создает html-страницы router*.html и index.html
def createRouterHTML(spos, projTitle, repPath, templates):
    args = {'project_title': projTitle}
    list_names = list()
    list_names.append({'router_name': 'router_sample', 'app_name': 'Приложения к акту отбора образцов'})
    list_names.append({'router_name': 'router_id', 'app_name': 'Приложения к протоколу идентификации'})
    list_names.append({'router_name': 'router_udf', 'app_name': 'Приложения НДВ'})
    # Отрисовка index.html
    with (repPath / projTitle / 'index.html').open('bw') as index:
        args['names'] = list_names
        template = TemplateLookup(directories=str(templates), input_encoding='utf-8', output_encoding='utf-8').get_template('index')
        index.write(template.render_unicode(data=args).encode('utf-8', 'replace'))
    args['spos'] = spos
    list_names[0]['link_name'] = 'root_sample'
    list_names[1]['link_name'] = 'root_id'
    list_names[2]['link_name'] = 'root'
    # Отрисовка router*.html
    for names in list_names:
        with (repPath / projTitle / 'udf' / (names['router_name'] + '.html')).open('bw') as outf:
            args['names'] = names
            template = TemplateLookup(directories=str(templates), input_encoding='utf-8', output_encoding='utf-8').get_template('router')
            outf.write(template.render_unicode(data=args).encode('utf-8', 'replace'))


# Функция getCoverage
# добавляет данные по покрытию для динамического анализа
def getCoverage(level, data, dbCon):
    cursor = dbCon.cursor()
    try:
        query = """SELECT rel.res * 100 / fun.co , rel_2.res * 100 / fun.co FROM \
(select count(*) as res from function where is_useful = 0) as rel,\
(select count(*) as co from function) as fun, \
(select count(*) as res from function where is_useful > 1) as rel_2"""
        cursor.execute(query)
        queryResult = cursor.fetchone()
        data['excessive_functions'] = queryResult[0]
        data['dynamic_binary_coverage'] = queryResult[1]
    except sqlite3.DatabaseError:
        print('getCoverage error 3 level!!!')

    if level == 2:
        try:
            query = """SELECT rel.res * 100 / line.co FROM \
(select count(*) as res from Line where is_useful > 1) as rel, \
(select count(*) as co from Line) as line"""
            cursor.execute(query)
            queryResult = cursor.fetchone()
            data['dynamic_function_coverage'] = queryResult[0]
        except sqlite3.DatabaseError:
            print('getCoverage error 2 level!!!')


# Функция generateQueries
# Формирует запросы к БД для статического анализа.
def generateQueries(level):
    queries = dict()
    if level <= 4:
        queries['sources'] = """select src.id_src, src.name || src.ext fullname, \
src.size_byte as len, src.cs as crc, '' as pad, 'Каталог' as param_name, \
src.path as param_value, s.count as param_count, \
'' from src, (select path, count(*) count from src group by path) as s \
where src.path = s.path order by param_value"""
        queries['binaries'] = """select bin.id_bin, bin.name || bin.ext as fullname, \
bin.size_byte as len, bin.cs as crc, \
'' as pad, 'Каталог' as param_name, bin.path as param_value, b.count as param_count, '' \
from bin, (select path, count(*) count from bin group by path) as b \
where bin.path = b.path order by param_value"""
        queries['bin-source-link'] = """select s.id_src, s.path || '\\' || s.name || s.ext as fullname, \
'' as pad, 'Исполняемый файл' as param_name, b.path || '\\' || b.name || b.ext as param_value, \
co.count as param_count, b.id_bin as param_id from src as s, bin as b, bin_src_link as bs, \
(select id_bin, count(*) count from bin_src_link group by id_bin) as co \
where b.id_bin = bs.id_bin and s.id_src = bs.id_src and co.id_bin = bs.id_bin order by bs.id_bin, s.id_src"""
        queries['exc-sources'] = """select src.id_src, src.name || src.ext fullname, \
src.size_byte as len, src.cs as crc, 'Каталог' as param_name, \
src.path as param_value, s.count as param_count, '' \
from src, (select path, count(*) count from src group by path) as s \
where src.path = s.path and src.is_useful = 0 order by param_value"""
        queries['bin-without-src'] = """select id_bin, path || name || ext fullname from bin \
where id_bin not in (select id_bin from bin_src_link)"""
    if level <= 3:
        queries['functions'] = """select f.name as name, f.type_param as parameters, \
f.ret_type as ret_type, f.def_offset as offset, f.count_line as lines, f.id as id, \
f.udb_id as udb_id, f.is_useful as chck, 'Файл' as param_name, \
f.def_file as param_value, fun.count as param_count, '' from function as f, \
(select def_file, count(*) count from function group by def_file) as fun \
where f.def_file = fun.def_file order by param_value, f.id"""
        queries['exc-functions'] = """select f.name as name, f.type_param as parameters, \
f.ret_type as ret_type, f.def_offset as offset, f.count_line as lines, f.id as id, \
f.udb_id as udb_id, 'Файл' as param_name, f.def_file as param_value, fun.count as param_count, '' \
from function as f, \
(select def_file, count(*) count from function where is_useful = 0 group by def_file) as fun \
where f.def_file = fun.def_file and f.is_useful = 0 order by param_value, f.id"""
        queries['fun-fun-link'] = """select f1.name, f1.def_file as source, \
f1.def_offset as offset, f1.id, 'Функция ' || f2.name as param_name, \
f2.def_file || ' (строка ' || f2.def_offset || ')' as param_value, \
co.count as param_count, f2.id as param_id from function f1, function f2, \
func_func_link as ffl, \
(select id_parent, count(*) count from func_func_link group by id_parent ) as co \
where f1.id = ffl.id_child and f2.id = ffl.id_parent and co.id_parent = f2.id order by f2.id"""
        queries['gVar'] = """select var.id, var.name, var.type as type, var.def_offset as offset, \
'' as pad, 'Файл' as param_name, var.def_file as param_value, co.count as param_count, '' \
from gVar var, (select count(*) as count, def_file from gVar group by def_file) as co \
where co.def_file = var.def_file order by param_value"""
        # TODO: поправить линки, когда нормальная вставка будет
        queries['gVar-fun-link'] = """select gVar.id, gVar.name, gVar.type, gVar.def_file, \
gVar.def_offset as offset, 'Файл ' || function.def_file as param_name, \
'Функция ' || function.name as param_value, '', function.id as param_id \
from gVar, function, gVar_Func_link as vfl \
where vfl.id_gVar = gVar.id and vfl.id_func = function.id order by param_value"""
    if level <= 2:
        queries['lin'] = """select li.id_line_in_func as num, li.id_global as glob, \
li.begin as offset, li.size as lines, '' as pad, fun.udb_id as udb_id, \
'Файл' as param_name, fun.def_file as param_value, coco.count as param_count, \
src.id_src as param_id, 'Функция' as param2_name, fun.name as param2_value, \
co2.count as param2_count, fun.id as param2_id from line as li, function as fun, src, \
(select sum(co.tada) as count, function.def_file from function, \
(select count(*) as tada, id_func from line group by id_func) as co \
where co.id_func = function.id group by function.def_file) as coco, \
(select count(*) as count, id_func from line group by id_func) as co2 \
where fun.id = li.id_func and src.path || '\\' || src.name || src.ext = fun.def_file and \
coco.def_file = src.path || '\\' || src.name || src.ext and co2.id_func = li.id_func \
order by param_id, param2_id limit 50000"""
        queries['lin-lin-link'] = """select '' as calling, lili.id_line2 as called, '' as pad, \
fun.udb_id as udb_id, 'Файл' as param_name, fun.def_file as param_value, '', src.id_src as param_id, \
'Функция' as param2_name, fun.name as param2_value, '', fun.id as param2_id, 'ЛУ' as param3_name, \
lili.id_line1 as param3_value, co.count as param3_count, '' \
from Line_Line as lili, src, function as fun, line as li, \
(select count(*) as count, id_func, id_line1 from Line_Line group by id_func, id_line1) as co \
where lili.id_func = li.id_func and lili.id_line1 = li.id_global \
and src.path || '\\' || src.name || src.ext = fun.def_file and fun.id = li.id_func \
and co.id_line1 = lili.id_line1 and co.id_func = lili.id_func order by param_id, param2_id limit 50000"""
    return queries


# Функция generateDynQueries
# Формирует запросы к БД для статического анализа.
def generateDynQueries(level):
    queries = dict()
    if level <= 3:
        percent = """select total.id_bin as id_bin, 100 * COALESCE(useful.total, 0) / total.total as percent \
from (select fun.id_bin, sum(fun.co) as total \
from (select bin.id_bin as id_bin, coco.co \
from (select count(*) as co, def_file from function group by def_file) as coco, src, bin, bin_src_link as bsl \
where bsl.id_bin = bin.id_bin and bsl.id_src = src.id_src and \
coco.def_file = src.path || '\\' || src.name || src.ext) as fun group by fun.id_bin) as total \
left outer join \
(select fun.id_bin, sum(fun.co) as total \
from (select bin.id_bin as id_bin, coco.co \
from (select count(*) as co, def_file from function where is_useful > 1 group by def_file) as coco, \
src, bin, bin_src_link as bsl \
where bsl.id_bin = bin.id_bin and bsl.id_src = src.id_src and coco.def_file = src.path || '\\' || src.name || src.ext) \
as fun group by fun.id_bin) as useful on total.id_bin = useful.id_bin"""
        queries['dyn_bin'] = """select bin.id_bin as id_bin, bin.name || bin.ext as fullname, \
bin.size_byte as len, bin.cs as crc, percent.percent as percent, percent.percent as barcontainer, \
'Каталог' as param_name, bin.path as param_value, b.count as param_count, '' \
from bin, (select path, count(*) count from bin where id_bin in (select id_bin from Bin_Src_Link) group by path) as b, \
(""" + percent + """) as percent \
where bin.path = b.path and percent.id_bin = bin.id_bin order by param_value"""
        queries['dyn_fun'] = """select f.name as name, f.ret_type as ret_type, f.def_offset as offset, \
f.count_line as lines, f.is_useful as chck, f.id as id, f.udb_id as udb_id, \
'Файл' as param_name, f.def_file as param_value, fun.count as param_count, '' from function as f, \
(select def_file, count(*) count from function group by def_file) as fun \
where f.def_file = fun.def_file order by param_value"""
        if level == 2:
            percent = """select total.id_func as id_func, COALESCE(useful.ttl, 0) * 100 / total as percent \
from (select id_func, count(*) as total from Line group by id_func) as total \
left outer join (select id_func, count(*) as ttl \
from Line where is_useful > 1 group by id_func) as useful on total.id_func = useful.id_func"""
            queries['dyn_fun'] = """select f.name as name, f.ret_type as ret_type, f.def_offset as offset, \
f.count_line as lines, f.is_useful as chck, f.id as id, percent.percent as percent, \
percent.percent as barcontainer, f.udb_id as udb_id, 'Файл' as param_name, \
f.def_file as param_value, fun.count as param_count, '' from function as f, \
(select def_file, count(*) count from function where id in (select id_func from Line) group by def_file) as fun, \
(""" + percent + """) as percent \
where f.def_file = fun.def_file and f.id = percent.id_func order by param_value, f.id"""
    return queries


# Функция createNames
# Задает расшифровку английских сокращений на русском.
def createNames():
    names = OrderedDict.fromkeys([
        'binaries', 'sources', 'bin-source-link', 'bin-without-src',
        'exc-sources', 'functions', 'exc-functions', 'fun-fun-link', 'lin',
        'lin-lin-link', 'gVar', 'gVar-fun-link', 'dyn_bin', 'dyn_fun'
        ])

    names['binaries']        = 'Список исполняемых файлов'
    names['sources']         = 'Список исходных файлов'
    names['bin-source-link'] = 'Связь исполняемых-исходных файлов'
    names['bin-without-src'] = 'Список исполняемых избыточных файлов'
    names['exc-sources']     = 'Список исходных избыточных файлов'
    names['functions']       = 'Список функциональных объектов'
    names['exc-functions']   = 'Список избыточных функциональных объектов'
    names['fun-fun-link']    = 'Связь функциональных объектов по управлению'
    names['lin']             = 'Список линейных участков'
    names['lin-lin-link']    = 'Связь линейных участков'
    names['gVar']            = 'Список информационных объектов'
    names['gVar-fun-link']   = 'Связь функциональных объектов по информации'

    names['dyn_bin']         = 'Покрытие по исполняемым файлам'
    names['dyn_fun']         = 'Покрытие по функциональным объектам'
    return names


def copyFile(src, dst):
    try:
        shutil.copyfile(src, dst)
    except IOError as error:
        print("Can't copy file! %s" % error.strerror)
    else:
        print("Copied image!")

# for testing
# if __name__ == '__main__':
#     main({'name': 'test_project', 'projects': ['kkvt, 1234, 3']})