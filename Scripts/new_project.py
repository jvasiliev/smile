import os
import re
import shutil
import configparser
import subprocess
import chardet
import codecs
import open_project

def get_lang():
    lang_dict = {'1' : 'C++','2' : 'C#', '3' : 'Java', '4' : 'Web', '5' : 'Pascal', '6' : 'Shell', '7' : 'Python', '8' : 'Perl', '9' : 'ASM'}
    lang_flag = False
    lang_list = []
    while not lang_flag:
        print('Укажите через запятую номера языков, используемых в проекте','1. C\C++','2. C#', '3. Java', '4. Web (JavaScript, PHP)', '5. Pascal(3lvl only)', '6. Shell (Edit srcTypes [])', '7. Python', '8. Perl', '9. Assembly (TASM)', 'Пример: 1,2', sep = '\n')
        user_str = input('>> ') + ','
        choice_list = user_str.split(',')
        choice_list.pop()
        for choice in choice_list:
            if choice not in lang_dict.keys():
                lang_flag = False
                break
            else:
                lang_flag = True
                lang_list.append(lang_dict[choice])
    return lang_list

def restore(path_pr):
    try:
        shutil.rmtree(path_pr)
    except Exception as err: 
        print('[WARNING]', 'Не удалось отменить предыдущие действия.', err, '[WARNING]', sep = '\n')
    else:
        print('Изменения отменены.\n')

def get_name():
    emb_name = ''
    while emb_name == '':
        emb_name = input('\nВведите наименование платы\n(Поддерживаемые виды - LPC32XX): ')
        if emb_name == '':
            print('Указано пустое наименование.\n')
            continue
        if emb_name != '':
            return emb_name

def get_lvl():
    while True:
        print('По какому уровню проверка:','2','3','4',sep = '\n')
        try: choise = int(input('>> '))
        except ValueError: continue

        if choise == 2:   return 2
        elif choise == 3:   return 3
        elif choise == 4:   return 4

def refactor(path_pr, mask_list):
    #перенос пустых фигурных скобок            void ololo() {   }
    try:
        files=os.walk(os.path.join(path_pr, 'Lab'))
        for file_wst in files:
            for file in file_wst[2]:
                #если тип файла поддерживается Astyle
                if ('*'+file[file.rfind('.'):]) in mask_list:
                    #то открываем файл
                    fin = open(os.path.join(path_pr, 'Lab', file_wst[0], file), 'a+')
                    code=''
                    fin.seek(0)
                    print('****************************************************************')
                    #двигаемся по стокам и ищем фигурные скобки
                    for line in fin:
                        flag=True
                        id_=line.find('{')
                        if id_!=-1:
                            _id=line.find('}')
                            if _id!=-1:
                                #если фигурные скобки найдены
                                for i in line[id_+1:_id-1]:
                                    if i.isspace()==False:
                                        flag=False
                                        break
                                if flag:
                                    #если между фигурными скобками нет символов
                                    if (_id-id_)==1:
                                        code+=line[:id_-1]+'\n'+line[id_]+'\n\n'+line[_id:]
                                    #если между фигурными скобками есть символы
                                    else:
                                        code+=line[:id_-1]+'\n'+line[id_:_id-1]+'\n\n'+line[_id:]
                                else:
                                    code+=line
                            else:
                                code+=line
                        else:
                            code+=line
                    fin.close()
                    fin = open(os.path.join(path_pr, 'Lab', file_wst[0], file), 'w')
                    fin.write(code)
                    fin.close()
    except:
        print('Error')
def insertBrackets(i,data):
    count,flag = 0,False
    for j in range(i,len(data)):
        if ( '{' in data[j].split()[0] ):
            count+=1
            flag = True
        if ( '}' in data[j].split()[0] ):
            count-=1
        if (flag == True and count == 0):
            return j


def addBrackets(path_pr, mask_list):
    #перенос пустых фигурных скобок            void ololo() {   }
    files=os.walk(os.path.join(path_pr, 'Lab'))
    for file_wst in files:
        for file in file_wst[2]:
            #если тип файла поддерживается Astyle
            if ('*'+file[file.rfind('.'):]) in mask_list:
                #то открываем файл
                fin = open(os.path.join(path_pr, 'Lab', file_wst[0], file), 'r')
                fin.seek(0)
                print('****************************************************************')
                #двигаемся по стокам и ищем фигурные скобки
                flag = False
                data = []
                for line in fin:
                    data.append( line )
                count_do,j = 0,0
                '''
                for i in range(len(data)-1):
                    if ( 'do' in data[i].split()[0] ):
                        if ( '{' not in data[i+1].split()[0] ):
                            data[i]+=('{\n')
                            count_do+=1
                            j = insertBrackets(i,data)
                            print ('11__',data[j+1])
                            data[j+1] = '\t}'+data[j+1]+'\n'
                            print('__',1)
                        if ( 'while' in data[i].split()[0] ):
                        print ('__2')
                        print (data[i].split()[-1][-1])
                        if ( ';' in data[i].split()[-1][-1] ): 
                            print ('___4')
                            if ()
                                data[i]=data[i].insert(0,'}')
                                print(2)
                    '''

                for i in range(len(data)-1):
                    if ( data[i].split()[0] in ['for', 'while', 'if'] and ';' not in data[i].split()[-1][-1]  ):
                        print ( data[i] )
                        if ( data[i+1].split()[0] in ['for', 'while', 'if', 'do'] ):
                            print ( data[i+1].split()[0] )
                            if ( 'do' in data[i+1].split()[0] ):
                                print('__000')
                            if  ( '{' not in data[i+1].split()[0] ):
                                 data[i]+=('{\n')
                                 i+=2
                                 if ( 'do' in data[i+1].split()[0] ):
                                    print('_____2_____\n\n\n\n\n\n\n\n')
                                    data[insertBrackets(i,data)]=('1}'+data[insertBrackets(i,data)])
                                 else:
                                    data[insertBrackets(i,data)]+=('}\n') 
                    '''
                    if ( 'do' in data[i].split()[0] ):
                        if ( '{' not in data[i+1].split()[0] ):
                            data[i]+=('{\n')
                    '''        
                            
                fin.close()
                fin = open(os.path.join(path_pr, 'Lab', file_wst[0], file), 'w')
                for line in data:
                    fin.write( line )
                fin.close()
def new():
    print('\n== Создание проекта ==')

    # ввод имени
    name_pr = ''
    while name_pr == '':
        name_pr = input('Для возврата введите \'b\' \nВведите имя проекта: ')
        if name_pr == '':
            print('Указано пустое имя.\n')
            continue
        if name_pr == 'b':
            return

    # формирование путей
    orign_path = os.getcwd()
    os.chdir('../')
    path_pr = os.path.join(os.getcwd(), 'Projects', name_pr)  # путь к проекту
    path_lab = os.path.join(os.getcwd(), path_pr, 'Lab') # путь к лабораторной версии
    path_orig = os.path.join(os.getcwd(), path_pr, 'Orig') # путь к "чистой" версии
    path_src = os.path.join(os.getcwd(), 'Sources', name_pr)  # путь к исходным текстам проекта
    os.chdir(orign_path)

    # возможно проект с таким именем уже существует
    if os.path.exists(path_pr): 
        print('Проект с таким именем уже существует.\n')
        return 

    # получаем необходимую информацию о проекте 
    level = get_lvl() 
    lang_list = get_lang()
    emb = ''
    embedded = ''
    while emb == '':
        emb = input('\nПроект задействует встриваемые платы?\n(Введите Y/N): ')
        if emb == '':
            print('Повторите ввод.\n')
            continue
        if emb == 'Y':
            embedded = get_name()
        else:
            embedded = 'No'
            continue
            
    # формируем "чистую" версию исходников  
    try:
        print('--- Копирование исходников в Orig ---')
        shutil.copytree(path_src, path_orig)
        print('---- Копирование в Orig завершено. ----')
        
        # определение и смена кодировок в исходниках на utf-8
        print('---- Преобразование кодировки исходников ----')
        src_ext_list = ['.c','.cs', '.h', '.cpp', '.hpp', 'hxx', 'cxx', '.java', '.php', '.js', '.PHP', '.JS', '.C', '.CS', '.H','.HPP', 'HXX', 'CXX', '.CPP', '.JAVA', '.PAS', '.pas', '.DPR', '.dpr', '.py', '.PY', '.pl', '.pm', '.cgi', '.asm', '.ASM']
        for root, dirs, files in os.walk(path_orig):
            for f in files:
                if os.path.splitext(f)[1] in src_ext_list:
                    src_path = os.path.join(root,f)
                    src_file = open(src_path,'rb')
                    result = chardet.detect(src_file.read())
                    src_file.close()
                    src_enc = result['encoding']
                    if 'ISO' in str(src_enc):
                        continue
                    if '1253' in str(src_enc):
                        continue
                    if '1255' in str(src_enc):
                        continue
                    if src_enc != 'utf-8':
                        with codecs.open(src_path, "r", src_enc) as sourceFile:
                            with codecs.open(src_path + ".tmp", "w", "utf-8") as destFile:
                                try:
                                    contents = sourceFile.read()
                                    print(sourceFile)
                                except (OSError, IOError) as err:
                                    print('[ERROR]', '-> Преобразование кодировок <-', err, '[ERROR]', sep = '\n')
                                    continue
                                destFile.write(contents)
                        #TODO: Разбить на два try блока
                        # т.к. исключение может возникнуть при удаление, тогда надо ставить паузу и просить закрыть файл
                        try:
                            os.remove(src_path)
                            os.rename(src_path + '.tmp', src_path)
                        except (OSError) as err:
                            print('[ERROR]', '-> Замена временного файла <-', err, '[ERROR]', sep = '\n')
        print('---- Преобразование кодировки завершено ----')

        # исправление astyle'ом
        mask_list = ['*.cs','*.cpp','*.c','*.h','*.java', '*.php', '*.js']

        for ext in mask_list:
            path_astyle = os.path.join(os.path.dirname(orign_path),'Resources', 'astyle.exe')
            subprocess.call([path_astyle,'--recursive','-A10','-H','-xe','--suffix=none',os.path.join(path_orig,ext)])
            subprocess.call([path_astyle,'--recursive','-A1','-H','--suffix=none',os.path.join(path_orig,ext)])

        '''addBrackets(path_pr, mask_list)
        for ext in mask_list:
            path_astyle = os.path.join(os.path.dirname(orign_path),'Resources', 'astyle.exe')
            subprocess.call([path_astyle,'--recursive','-A10','-H','-xe','--suffix=none',os.path.join(path_pr,ext)])
            subprocess.call([path_astyle,'--recursive','-A1','-H','--suffix=none',os.path.join(path_pr,ext)])
        '''
        os.makedirs(os.path.join(path_pr, 'Databases'))
        refactor(path_orig, mask_list)

        print('---- Копирование исходников в Lab ----')
        shutil.copytree(path_orig, path_lab)
        print('---- Копирование в Lab завершено ----')

    except (shutil.Error, OSError, IOError) as err:
        print('[ERROR]', '-> Cоздания проекта <-', err, '[ERROR]', sep = '\n')
        restore(path_pr)
        return
    print('--- Копирование завершено ---')

# создаем файл кофиг для проекта
    cfg = configparser.SafeConfigParser()
    cfg.add_section('common')
    cfg.set('common', 'level', str(level))
    cfg.set('common', 'lang', ','.join(lang_list))    
    cfg.set('common', 'embedded', str(embedded))
    with open(os.path.join(path_pr, name_pr + '.conf'), 'w+') as config:
        cfg.write(config)

# завершаем создание проекта
    print('== Создание проекта завершено ==\n')
    open_project.open(name_pr)
    return
