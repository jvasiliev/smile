import configparser
import sqlite3
import os
from . import ins_mark, an_trace

def database_open(name):
    try:
        db = sqlite3.connect(name)
    except sqlite3.DatabaseError as err:
        log.write("Ошибка при открытии базы данных sqlite.\n"\
                "Error: %s\n" % err)
        return None
    return db

def dynamic_analysis(name_pr, path_pr):
# считывание файла конфига на наличие уровня анализа
    cfg = configparser.SafeConfigParser()
    cfg.read(os.path.join(path_pr, name_pr + ".conf"))
    try:
        level = cfg.getint('common', 'level')
    except (configparser.NoOptionError, configparser.NoSectionError) as err:
        print('Конфигурационный файл был изменен.', 'Error: %s' % err, sep = '\n')
        return False
#открытие нашей базы проекта
    sdb = database_open(path_pr+"/Databases/"+name_pr+".sdb")
    if sdb == None: return False
#выбор действий при динамическом анализе
    while True:
        print('Динамический анализ','1. Вставить датчики', '2.Анализировать трассу', '3. Выход', sep='\n')
        try: choice = int(input('>> '))
        except ValueError: continue
        if choice == 1: ins_mark.Insert_marker(sdb, level)
        elif choice == 2: an_trace.Analyizing(sdb, level)
        elif choice == 3: break
    return True