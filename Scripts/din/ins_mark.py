#coding: utf-8
import sqlite3
import os
import stat
import logging
'''def markr_c++(file_name):
    header_file = open(file_name,'w')
    include = '#include <stdio.h>\n'
    func = 'void datchik___(int id,int id_src, bool flag)\n'\
            '{\n'\
            '    FILE *of = fopen("TRACE_%s_TRACE","w");\n'\
            '    fprintf(of,"%d :: %d\n", id, id_src);\n'\
            '}\n\n'

'''
# если запустить еще раз, то будет повторная вставка датчиков, в позиции из бд. (кол-во датчиков увеличится. вставка будет не правильной)


def Insert_marker(con, level, embed):
    # запись в лог файл, в случае ошибок
    logging.basicConfig(filename = 'marker.log',level=logging.ERROR)
    query_file_name = 'SELECT * FROM Src;'
    query_func = 'SELECT id, def_offset FROM Function WHERE def_file="%s" and count_line<>0 and name<>"set" and name<>"get";'
    query_line = 'SELECT id_global, begin, size FROM Line WHERE id_func=%d ORDER BY begin DESC;'
#   try:
#       con = sqlite3.connect('task.sdb')
#   except sqlite3.DatabaseError as err:
#       logging.error('Error: %s' %err)
#       return
    cur = con.cursor()
    cur.execute(query_file_name)
    try:
        all_path = cur.fetchall()
    except Exception as err:
        logging.error('Error: %s' %err)
        print('ERRRRRRRRRRRRRRRRRRRRRRRRRRRRROOOOOOOR')
        return
    # датчик и подключения библиотеки
    datchik,include = '',''
    for a_file in all_path:
        datchik_line = ''
        web_marker = ''
        file_name = str(a_file[3]+'\\'+a_file[1]+a_file[2])
        if level in [2, 3]:
            if (embed == 'LPC32XX') and (a_file[2] in ['.h','.cpp','.c']):
                datchik = '\tuart_output((UNS_8*)"%d :: %d");\n'
                include = ''
            if a_file[2] in ['.h','.cpp','.c'] and (embed == 'No'):
                #datchik = '\tstatic int markname[] = {0x%08x, 0x%08x, %s}; '\
                #            '\tif(markname[2] != 0xFACEDEAD)'\
                datchik = '\tif(1)'\
                            '\t{'\
                            '\tFILE *of = fopen("TRACE_+_TRACE.txt","at");'\
                            '\tfprintf(of,"%d :: %d\\n");'\
                            '\tfclose(of);'\
                            '\t}\n'      
                include = '#include <stdio.h>\n'
            if a_file[2] in ['.cs']:
                #datchik =   '\tuint [] markname = new uint{0x%08x, 0x%08x, %s};'\
                #            '\tif (markname[2] != 0xFACEDEAD)'\
                #            '\ttry{'\
                datchik = 'if(true)'\
                            '\t{'\
                            '\tSystem.IO.StreamWriter swriter = new System.IO.StreamWriter("TRACE_+_TRACE.txt",true);'\
                            '\tswriter.WriteLine("%d :: %d\\n");'\
                            '\tswriter.Close();}\n'\
                # datchik =   '\tThread tr_%d = new Thread(new ThreadStart(delegate() { if(true)'\
                            # '\t{'\
                            # '\tSystem.IO.StreamWriter swriter = new System.IO.StreamWriter("TRACE_+_TRACE.txt",true);'\
                            # '\tswriter.WriteLine("%d :: %d\\n");'\
                            # '\tswriter.Close();}}));\n'\
                            # '\ttr_%d.Start();\n'\
                            # '\ttr_%d.Join();\n'
                #datchik =   '\tif(true)'\
                #datchik = '{\n}'
                #include = 'using System.IO;'
                #include = 'using System.Threading;'
                include = ''
            if a_file[2] in ['.js']:
                datchik = ''
                web_marker = ''
                #datchik = '\tvar __fs = require(\'fs\'); \t  __fs.appendFileSync("TRACE_+_TRACE.txt", "%d :: %d\\n"); \n'
                #web_marker = 'var __fs = require(\'fs\');  __fs.appendFileSync(\'TRACE_src_TRACE.txt\', \'%d\');'
            if a_file[2] in ['.php']:
                datchik = ''
                web_marker = ''
                #datchik = '\tfile_put_contents("TRACE_+_TRACE.txt", "%d :: %d\\n", FILE_APPEND); \n'
                #web_marker = '<?php file_put_contents("TRACE_src_TRACE.txt", "%d\\n", FILE_APPEND);?>\n'
            if a_file[2] in ['.java']:
                datchik = '\t{if(true)'\
                            '\t{'\
                            '\ttry{'\
                            '\tjava.io.PrintWriter p_writer = new java.io.PrintWriter(new java.io.FileWriter("TRACE_+_TRACE.txt", true));'\
                            '\tp_writer.println("%d :: %d\\n");'\
                            '\tp_writer.close();}\n'\
                            '\t\tcatch(Exception e)\t{}}}\n'
                            
        
        if level == 2:
            if (embed == 'LPC32XX') and (a_file[2] in ['.h','.cpp','.c']):
                datchik_line = '\tuart_output((UNS_8*)"%d");\n'
            if a_file[2] in ['.h','.cpp','.c'] and (embed == 'No'):
                datchik_line = '\tif(%s)'\
                            '{'\
                            '\tFILE *of = fopen("TRACE_-_TRACE.txt","at");'\
                            '\tfprintf(of,"%d\\n");'\
                            '\tfclose(of);'\
                            '\t}\n'
            if a_file[2] in ['.cs']:
                datchik_line = '\tif(%s)'\
                            '\t{'\
                            '\tSystem.IO.StreamWriter sw = new System.IO.StreamWriter("TRACE_-_TRACE.txt",true);'\
                            '\tsw.WriteLine("%d\\n");'\
                            '\tsw.Close();'\
                            '\t}\n'
            if a_file[2] in ['.java']:
                datchik_line = '\t{if(%s)'\
                            '\t{'\
                            '\ttry{'\
                            '\tjava.io.PrintWriter p_writer = new java.io.PrintWriter(new java.io.FileWriter("TRACE_+_TRACE.txt", true));'\
                            '\tp_writer.println("%d\\n");'\
                            '\tp_writer.close();}\n'\
                            '\t\tcatch(Exception e_%d)\t{}}}\n'
            if a_file[2] in ['.js']:
                datchik_line = '\t var _fs = require(\'fs\'); \t  _fs.appendFileSync(\'TRACE_-_TRACE.txt\', \'%d\\n\'); \t'
            if a_file[2] in ['.php']:
                datchik_line = '\t <?php file_put_contents(\'TRACE_-_TRACE.txt\', \'%d\\n\', FILE_APPEND);?> \t'                            

        position = []
        try:
            cur.execute(query_func%file_name)
            position = cur.fetchall()
        except sqlite3.DatabaseError as err:
            print('Не найти файлы')
            logging.error('Error: %s' %err)
        try:
            file_in = open(file_name, 'r', encoding='cp866')
        except IOError as err:
            logging.error('Error: can`t open file = %s for read' %file_name)
            print('Не вставить файлы')
            continue
        file_buf,flag = [],False
        print(file_name)
        for line in file_in:
            file_buf.append(line)
        # для уровня 2.
        posistion_line = []
        position.sort(key = lambda x:x[1], reverse = True)
        # добавить вставку в 1 линейный участок (тянуть из базы)
        # вставка датчика
        count = 0
        for pos in position:
            count+=1
            if level == 2:
                try:
                    cur.execute(query_line%pos[0])
                    posistion_line = cur.fetchall()
                    posistion_line.sort(key = lambda x:x[1], reverse = True)
                except sqlite3.DatabaseError as err:
                    logging.error('Error: %s' %err)
                for pos_line in posistion_line:
                    try:
                        if a_file[2] in ['.java']:
                            file_buf.insert(pos_line[1]-1,(datchik_line%('true',pos_line[0],pos_line[0])))
                        else:
                            if embed == 'LPC32XX':
                                file_buf.insert(pos_line[1]-1,(datchik_line%(pos_line[0])))
                            else:
                                file_buf.insert(pos_line[1]-1,(datchik_line%('true',pos_line[0])))
                    except TypeError as err:
                        logging.error('Error: may be wrong language... %s'%err)
            try:
                count = 0
                try:
                    while '{' not in file_buf[pos[1]+count]:
                        count += 1
                    count += 1
                except:
                    count = 1
                #next line has error
                #file_buf.insert(pos[1]+count,(datchik%(pos[0],a_file[0],'0xDEADFACE',pos[0],a_file[0])))
                # if a_file[2] in ['.cs']:
                            # file_buf.insert(pos[1]+count,(datchik%(pos[0],pos[0],a_file[0],pos[0],pos[0])))
                file_buf.insert(pos[1]+count,(datchik%(pos[0],a_file[0])))
                print(pos[1], pos[1]+count)
                print('===>', datchik)
            except TypeError as err:
                logging.error('Error: may be wrong language... %s'%err +'\n\t\t' + file_name + '\n\t\t' +str(a_file[0])+'\n\t\t'+str(pos[0]))
        if count:
            file_buf.insert(0,include)
        print('ID_src: ', a_file[0], type(a_file[0]) is int)
        try:
            file_buf.insert(0, web_marker%a_file[0])
        except TypeError as err:
            logging.error('Not a web source file: ' + file_name)
        file_in.close()
        try:
            if not os.access(file_name, os.W_OK):
                os.chmod(file_name, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO)
            os.remove(file_name)            
            #return
            file_out = open(file_name,'w', encoding='cp866')
        except OSError as err:
            logging.error('Can`t delete this file %s' %file_name, err, '\n')
        except IOError as err:
            logging.error('Can`t open file %s' %file_name, err, '\n')
        for line in file_buf:
            try:
                file_out.write(line)
            except IOError as err:
                logging.error('Can`t write to file %s' %file_name)
        file_out.close()
