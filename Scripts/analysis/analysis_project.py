from .level_4 import insert_marker
from .level_4 import find_marker
from .level_4 import src_bin_table
from .level_3 import main_udb

import os
import configparser
import understand
import sqlite3
import subprocess
import shutil
import errno
import time

def create_udb(name_pr, path_pr,sdb_path, lang_list):
    strin=''
    env_path = os.getenv('PATH')
    path_list = env_path.split(';')
    path_to_und = ''
    filename_list = []
    udb_path = os.path.join(path_pr,'Databases',name_pr + '.udb')
    for elem in path_list:
        if 'SciTools' in elem:
            path_to_und = os.path.join(elem,'und.exe')
            path_to_und = path_to_und.replace('/','\\')
            break
    con = sqlite3.connect(sdb_path)
    cur = con.cursor()
    try:
        query = 'select path, name, ext from Src where is_useful=1;'
        cur.execute(query)
        filename_list = []
        for path, name, ext in cur.fetchall():
            filename_list.append(os.path.join(path,name + ext))
    except:
        print('Ошибка при работе с sqlite3')
    cur.close()
    con.close()
    with open(os.path.join(path_pr,'Databases','filelist.lis'),'w') as filelist:
        for filename in filename_list:
            filelist.write(filename + '\n')
    try:
        for tmp in lang_list:
            strin = strin + ' ' + tmp
        exec_str=path_to_und + ' create' + ' -db ' + udb_path + ' -languages' + strin + ' add ' + '@' + os.path.join(path_pr,'Databases','filelist.lis') + ' analyze' + ' -all'
        subprocess.call(exec_str)
    except ValueError:
        print('Ошибка в первом вызове')
        return False
    try:
        exec_str = path_to_und + ' settings' + '-FileEncoding UTF-8 ' + udb_path
        subprocess.call(exec_str)
    except ValueError:
        print('Ошибка во втором вызове')
        return False
    return True

#Первый вариант перегрузки rmtree
def error_handler(func, path, execinfo):
    import stat
    e = execinfo[1]
    if e.errno == errno.ENOENT or not os.path.exists(path):
        return              # path does not exist - treat as success
    if func in (os.rmdir, os.remove) and e.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
        func(path)          # read-only file; make writable and retry
    else:
        raise e
            
#Перегрузка метода rmtree 
#для удаления файлов
#с правами только на чтение
# def error_handler(func, path, exc_info):
    # import stat
    # if not os.access(path, os.W_OK):
        # os.chmod(path, stat.S_IWUSR)
        # func(path)
    # else:
        # raise    
    
def analysis(name_pr, path_pr, path_lab, path_orig):

# читаем файл конфиг
    cfg = configparser.SafeConfigParser()
    cfg.read(os.path.join(path_pr, name_pr + ".conf"))
    #путь к архиватору
    path_to_freearc = "C:\\Smile\\Resources\\freearc\\"
# секция common
    lang_list = []
    try:
        level = cfg.getint('common', 'level')
        lang_str = cfg.get('common', 'lang')
        lang_list = lang_str.split(',')
        print('Lang_list :', lang_list)
    except (configparser.NoOptionError, configparser.NoSectionError) as err:
        print('Конфигурационный файл был изменен.', 'Error: %s' % err, sep = '\n')
        return

# секция src_bin
    print('\n== Таблицы Src и Bin ==')
    if not cfg.has_section('src_bin'):
        cfg.add_section('src_bin')

    # заполнение таблицы Src, Bin
    try:
        src = cfg.getint('src_bin', 'src')
        bin = cfg.getint('src_bin', 'bin')
    except configparser.NoOptionError as err:
        src, bin, path_to_freearc = src_bin_table.write_table(name_pr, path_pr, lang_list)
        cfg.set('src_bin', 'src', str(src))
        cfg.set('src_bin', 'bin', str(bin))
        with open(os.path.join(path_pr, name_pr + '.conf'), 'w+') as config:
                cfg.write(config)
    print('Исходников: %i' % src, 'Исполняемых: %i' % bin, sep = '\n')


    if level <= 4:
        print('\n== Анализ проекта по 4 уровню контроля ==')
        # секция four_level
        if not cfg.has_section('four_level'):
            cfg.add_section('four_level')

        # вставка маркеров
        try:
            mark = cfg.getboolean('four_level', 'mark')
            if mark == False: raise configparser.NoOptionError('four_level', 'mark')
            ins_mark = cfg.getint('four_level', 'ins_mark')
        except configparser.NoOptionError as err:
            mark, ins_mark = insert_marker.insert(name_pr, path_pr)
            cfg.set('four_level', 'mark', str(mark))
            cfg.set('four_level', 'ins_mark', str(ins_mark))
            with open(os.path.join(path_pr, name_pr + '.conf'), 'w+') as config:
                cfg.write(config)
        print('Вставлено маркеров: %i' % ins_mark)

        # если вставка с ошибками, то к идентификации не приступаем
        if mark == False:
            print('В процессе вставки маркеров произошла ошибка.')
            return

        # идентификация маркеров
        try:
            find = cfg.getboolean('four_level', 'find')
            if find == False: raise configparser.NoOptionError('four_level', 'find')
            find_mark = cfg.getint('four_level', 'find_mark')
        except configparser.NoOptionError as err:

            # обязательная перекомпиляция проекта
            input('Перекомпилируйте проект.')
            find, find_mark = find_marker.find(name_pr, path_pr, path_to_freearc)
            cfg.set('four_level', 'find', str(find))
            cfg.set('four_level', 'find_mark', str(find_mark))
        print('Идентифицировано маркеров: %i' % find_mark)

        if find == False:
            print('В процессе идентификации маркеров произошла ошибка.')
        
        # записываем информацию в конфиг
        with open(os.path.join(path_pr, name_pr + ".conf"), "w") as config:
            cfg.write(config)

        print('== Завершен анализ по 4му уровню ==\n')
    
        #копирование Orig->Lab для замены исходников с датчиками на чистые
        try:
            shutil.rmtree(path_lab, onerror=error_handler)
        except (shutil.Error, OSError, IOError) as err:
            log = open('log_clean_lab.txt', 'w')
            log.write('Error\n')
            print('[ERROR]', '-> Очистка папки Lab <-', err, '[ERROR]', sep = '\n')
        try:
            shutil.copytree(path_orig, path_lab)
        except (shutil.Error, OSError, IOError) as err:
            print('[ERROR]', '-> Копирование исходников из Orig <-', err, '[ERROR]', sep = '\n')

        if not os.path.isfile(os.path.join(path_pr,'Databases',name_pr+'.udb')):
            cfg.set('common', 'lang', ','.join(lang_list))
            if not create_udb(name_pr, path_pr, os.path.join(path_pr,'Databases',name_pr+'.sdb'), lang_list):
                print('Не удалось создать базу Understand')
            with open(os.path.join(path_pr, name_pr + '.conf'), 'w+') as config:
                cfg.write(config)
        input("check UDB")

    if level <=3:
        if not cfg.has_section('third_level'):
            cfg.add_section('third_level')
        #секции для Ф. О.
            cfg.set('third_level', 'func', str(False))
            cfg.set('third_level','func_id', '0')
            
            cfg.set('third_level','func_func', str(False))
            cfg.set('third_level','LfId_for_linking', '0')
        #секции для И. О.
            cfg.set('third_level', 'gVar', str(False))
            cfg.set('third_level','gVar_id', '0')
            
            cfg.set('third_level','gVar_func', str(False))
            cfg.set('third_level','LgId_for_linking', '0')
        
        if not cfg.has_section('second_level'):
            cfg.add_section('second_level')
        #секции для линейных участков
            cfg.set('second_level', 'line', str(False))
            cfg.set('second_level','lf_for_line', '0')
            
        main_udb.main_udb(name_pr,path_pr,lang_list, level, cfg)