import understand
import os

def draw_function(func, name_pr, log):
    log.write("\n  Создание блок-схемы функции %s" % func.name())
    # берем путь
    path = str(os.getcwd())
    # и убираем всё лишнее ;)
    # после цикла в path должно быть %SmileDir%\src
    # for i in range(2):
    path = os.path.join(path[:path.rfind("\\")], "Reports", "_rc", name_pr )
    # создание папок
    #path += "\\Reports\\_rc\\" + name_pr 
    if os.path.exists(path) == False:
        try:
            os.makedirs(path)
        except os.error as err:
            log.write("Ошибка при создании директорий! \n"\
                "Error: %s" % err)
    # рисование блок-схем
    try:
        func.draw("Control Flow", os.path.join(path, str(func.id())+".png"))
    except understand.UnderstandError as err:
        log.write("Ошибка при рисовании блок-схемы" + func.name() + " \n"\
            "Error: %s" % err)