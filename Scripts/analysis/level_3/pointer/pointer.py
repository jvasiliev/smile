﻿import sqlite3
def write_pointer_table(udb, db, cur):
    log = open('log_pointer.txt', 'w')
    log.write('### Pointer ###\n')

    str_INSERT = 'INSERT INTO Pointer(id_func_1, id_func_2) VALUES (?, ?);'
    str_FIND_func = 'SELECT id_func_sql FROM TEMP_Func WHERE id_func_udb=%d;'
    str_FIND_link = 'SELECT id_func_1, id_func_2 FROM Pointer WHERE id_func_1=%d;'

    id_func_1, id_func_2 = 0, 0
    for func in udb.ents('C Function ~Unknown ~Unresolved'):
        try:
            cur.execute(str_FIND_func % func.id())
        except sqlite3.DatabaseError as err:
            log.write('Ошибка в процессе поиска id_func_sql для %s.\Error: %s\n' % (func.simplename(), err))
        else:
            try:
                id_func_1 = cur.fetchone()[0]
            except TypeError:
                continue

        for ref in func.refs(''):
            if ref.kindname() == 'Pointer':
                id_ent_udb = ref.ent().id()
                try:
                    cur.execute(str_FIND_func % id_ent_udb)
                except sqlite3.DatabaseError as err:
                    log.write('Ошибка в процессе поиска id_func_sql для %s.\Error: %s\n' % (func.simplename(), err))
                else:
                    try:
                        id_func_2 = cur.fetchone()[0]
                    except TypeError:
                        continue

                try:
                    cur.execute(str_FIND_link % id_func_2)
                except sqlite3.DatabaseError as err:
                    log.write('Ошибка в процессе поиска id_func_1 и id_func_2.\nДанные:\n\t%s\n\t%s.\nError: %s\n' % (func.simplename(), ref.ent().simplename(), err))
                else:
                    try:
                        id_funcs = cur.fetchone()[0]
                    except TypeError:
                        try:
                            cur.execute(str_INSERT, [id_func_1, id_func_2])
                        except sqlite3.DatabaseError as err:
                            log.write('Ошибка при записи данных.\nДанные:\n\t%s\n\t%s.\nError: %s\n' % (func.simplename(), ref.ent().simplename(), err))
                        else:
                            db.commit()

    log.write('### Запись завершена ###\n')
    log.close()