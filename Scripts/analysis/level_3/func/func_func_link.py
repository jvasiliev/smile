import sqlite3

def write_func_func_link_table (udb, db, cur, lang_list, lid):
    print('write_func_func_link_table')
    log = open('log_func_func_link.txt', 'w')
    log.write('### Func_Funk_Link ###\n')

    str_FIND_func = 'SELECT id FROM Function WHERE udb_id=%d;'
    str_INSERT_result = 'INSERT OR REPLACE INTO Func_Func_Link (id_parent, id_child) VALUES (?, ?);'
    tmp_data = []
    data = []
    cur_id = 0
    for lang in lang_list:

        if 'C++' == lang:
            opt_ents = 'C Function ~Unknown ~Unresolved'
            opt_calls = 'C Call'
        elif 'Java' == lang:
            opt_ents = 'Java Method ~Unknown ~Unresolved'
            opt_calls = 'Java Call'
        elif 'C#' == lang:
            opt_ents = 'C# Method ~Unknown ~Unresolved'
            opt_calls = 'C# Call'
        elif 'Pascal' == lang:
            opt_ents = 'Pascal Procedure ~Unknown ~Unresolved ~Virtual, Pascal Function ~Unknown ~Unresolved'
            opt_calls = 'Pascal Call'
        elif 'Web' == lang:
            opt_ents = 'Web Javascript Function ~Unresolved, Web Php Function ~Unresolved'
            opt_calls = 'Web Javascript Call, Web Php Call'        
        elif lang == 'Python':
            opt_ents = 'Python Function ~Unresolved'
            opt_calls = 'Python Call'

        id_parent, id_child = 0, 0
        for func in udb.ents(opt_ents):
            if (lang in ['C#', 'Python']) and ('Standard' == func.library()):
                continue
            #print('1 - id_fo -', func.id())
            try:
                cur.execute(str_FIND_func % func.id())
            except sqlite3.DatabaseError as err:
                log.write('Ошибка в процессе поиска id_func_slq для %s.\nError: %s\n' % (func.simplename(), err))
                continue
            else:
                try:
                    id_parent = cur.fetchone()[0]
                    if id_parent <= lid:
                        continue
                    else: cur_id = id_parent
                except TypeError:
                    continue

            if 0 == len(func.refs(opt_calls)):
                continue
            
            id_ent_udb = []
            for ref in func.refs(opt_calls):
                id_ent_udb.append((ref.ent()).id())
            
            change_list = list(set(id_ent_udb))

            for id in change_list:
                #id_ent_udb = (ref.ent()).id()
                try:
                    cur.execute(str_FIND_func % id)
                except sqlite3.DatabaseError as err:
                    log.write('Ошибка в процессе поиска id_func_sql для %s.\nError: %s\n' % (func.simplename(), err))
                try:
                    id_child = cur.fetchone()[0]
                    print('Id_parent: %d \t\t Id_child: %d' % (id_parent, id_child))
                    try:
                        cur.execute(str_INSERT_result, [id_parent, id_child])
                        db.commit() 
                    except sqlite3.DatabaseError as err:
                        log.write('Ошибка во время добавления в таблицу func_func_link \n')
                        log.write('Error: %s \n' % err)
                except TypeError:
                    continue

    log.write('### Запись завершена ###\n')
    log.close()
    return True, cur_id