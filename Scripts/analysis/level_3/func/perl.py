import os
import sqlite3

def find_fo(code, cur, id_sub_):
	id_sub_ = code.find('sub ', id_sub_)
	id_braket_ = code.find('{', id_sub_)
	fo_name = code[id_sub_+4:id_braket_]
	print(fo_name)

def fo_table(cur, src_list):
	for src_file in src_list:
		src = os.path.join(src_file[0], src_file[1] + src_file[2])

		f = open(src, 'r')
		code = f.read()
		f.close()

		find_fo(code, cur, 0)

def perl_func(name_pr, path_pr):
	print('------ Perl FO Tbale ------')
	con = sqlite3.connect( os.path.join(path_pr, 'Databases', name_pr + '.sdb') )
	cur = con.cursor()

	cur.execute('SELECT path, name, ext FROM Src;')
	src_list = cur.fetchall()

	fo_table(cur, src_list)