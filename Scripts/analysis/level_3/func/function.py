﻿import sqlite3
from ..level_2 import line
from ..level_2 import graph

# У меня было 2 пакета травы
# Да, это - быдлокод, потому что надо делать проект
# производит поиск процедур в листинге кода ассемблера (тестил на TASM'e поэтому за другие диалекты не ручаюсь)
# поиск произодится по ключевым токенам: 'proc near' и 'endp'
# Так же реализован поиск связей процедур в пределах одного листинга
# и поиск ЛУ
def del_empty(array):
    out = []
    for key in array:
        if len(key) != 0:
            out.append(key)
    return out

def write_db(cur, FO_LU, level):
    fun_INSERT = 'INSERT OR REPLACE INTO Function (id, name, count_line, def_file, def_offset, dec_file, is_useful) VALUES (%s, \'%s\', %s, \'%s\', %s, \'%s\', %s);'
    ffl_INSERT = 'INSERT OR REPLACE INTO Func_Func_Link (id_parent, id_child) VALUES (%s, %s);'
    fun_USEFUL = 'UPDATE Function SET is_useful = 1 WHERE id IN (SELECT id_parent FROM Func_Func_Link) OR id IN (SELECT id_child FROM Func_Func_Link);'
    lin_INSERT = 'INSERT OR REPLACE INTO Line (id_func, id_line_in_func, id_global, begin, size, is_useful) VALUES (%s, %s, %s, %s, %s, %s);'
    lll_INSERT = 'INSERT OR REPLACE INTO Line_Line (id_line1, id_line2, flag_return, id_func) VALUES (%s, %s, %s, %s);'
    lin_UPDATE = 'UPDATE Line SET is_useful = 1 WHERE id_global IN (SELECT id_line1 FROM Line_Line) OR id_global IN (SELECT id_line2 FROM Line_Line);'
    UPDATE = 'UPDATE Function SET is_useful = 1 WHERE id IN (SELECT id_func FROM Line WHERE is_useful = 1);'
    try:
        for func in FO_LU[0]:
            cur.execute(fun_INSERT % (func[0], func[1], func[2], func[3], func[4], func[5], func[6]))
    except:
        print(fun_INSERT % (func[0], func[1], func[2], func[3], func[4], func[5], func[6]))
    try:
        for ffl in FO_LU[1]:
            cur.execute(ffl_INSERT % (ffl[0], ffl[1]))
    except:
        print(ffl_INSERT % (ffl[0], ffl[1]))
    cur.execute(fun_USEFUL)
    if level == 2:
        try:
            for line in FO_LU[2]:
                cur.execute(lin_INSERT % (line[0], line[1], line[2], line[3], line[4], line[5]))
        except:
            print(lin_INSERT % (line[0], line[1], line[2], line[3], line[4], line[5]))
        try:
            for lll in FO_LU[3]:
                cur.execute(lll_INSERT % (lll[0], lll[1], lll[2], lll[3]))
        except:
            print(lll_INSERT % (lll[0], lll[1], lll[2], lll[3]))
        cur.execute(lin_UPDATE)
        cur.execute(UPDATE)

def line_asm(cur, body, offset, id_func, id_global, lines, lll, jmp, goto):
    # словарь токенов
    dict = ['JZ', 'JE', 'JNZ', 'JNE', 'JC', 'JNAE', 'JB', 'JNC', 'JAE', 'JNB', 'JP', 'JNP', 'JS',\
            'JNS', 'JO', 'JNO', 'JA', 'JNBE', 'JNA', 'JBE', 'JG', 'JNLE', 'JGE', 'JNL', 'JL', 'JNGE',\
            'JLE', 'JNG', 'JCXZ', 'JECXZ', 'JMP', 'jz', 'je', 'jnz', 'jne', 'jc', 'jnae', 'jb', 'jnc', 'jae',\
            'jnb', 'jp', 'jnp', 'js', 'jns', 'jo', 'jno', 'ja', 'jnbe', 'jna', 'jbe', 'jg', 'jnle',\
            'jge', 'jnl', 'jl', 'jnge', 'jle', 'jng', 'jcxz', 'jecxz', 'jmp']
    #print("\n\n\n\n***************** id_func: %d *****************\n\n" % id_func)
    counter_offset = -1 # потому что смещение на первой строке должно быть равно 0
    size = 0
    id_line_in_func = 0
    for line in body.split("\n"):
        id_comment = line.find(";")
        id_goto = line.find(":")
        counter_offset += 1
        size += 1
        #print("%d   %d:\t%s" % (offset+counter_offset, size, line))
        for token in dict:
            id_token = line.find(token)
            # если найден токен и при этом он вне комментария
            if (id_token != -1) & ((id_token < id_comment) | (id_comment == -1)):
                id_line_in_func += 1
                id_global += 1
                lines.append([id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size, 0])
                #print("-----------------------------------------------------------------",\
                #        id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size, 0)
                size = 0
                # заполняю массив переходов
                # костылирую (удаление пустых элементов массива)
                dist = del_empty(line[line.find(token):id_comment].split(' '))
                if len(dist) > 1:
                    jmp.append([id_global, dist[-1], id_func])
                    #print("---------------- %d: %s, %d" % (id_global, dist[-1], id_comment))
                    #input()
                break
        # если не совпадает с предидущим ЛУ
        # и найдена метка
        # и  { метка не закомментирована (если есть комментарий) и между коментарием и меткой нет цифр/букв
        # или (если комментарий отсутствует) между меткой и концом строки нет цифр/букв}
        if (size != 1) & \
        (id_goto != -1) & \
        ( ((line[id_goto+1:id_comment].isspace()) & (id_goto < id_comment)) | \
        ((id_comment == -1) & ( (line[id_goto+1:].isspace()) | (line[id_goto+1:] == "") )) ):
                
            id_line_in_func += 1
            id_global += 1
            lines.append([id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size-1, 0])
            #print("-----------------------------------------------------------------",\
            #        id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size-1, 0)
            # заплняю "очевидные" связи ЛУ
            if id_line_in_func != 1:
                lll.append([id_global-1, id_global, 0, id_func])
            #print("-------------------- %d: %s" % (id_global, line[:id_goto].replace(" ", "")))
            # заподняю масив меток
            goto.append([id_global, line[:id_goto].replace(" ", ""), id_func])
            size = 1

    # последний ЛУ в ФО (если в последней строке ФО нет токена)
    if size != 0:
        id_line_in_func += 1
        id_global += 1
        lines.append([id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size, 0])
        #print("-----------------------------------------------------------------",\
        #        id_func, id_line_in_func, id_global, offset+counter_offset-size+1, size, 0)
        # заплняю "очевидные" связи ЛУ
        if id_line_in_func != 1:
            lll.append([id_global-1, id_global, 1, id_func])
        size = 0
    
    return id_global, lines, lll, jmp, goto


def func_asm(cur, name_pr, level):
    # так уж вышло, что на момент написания кода, при пребразовании исходников в utf-8 все плывет к е6.не-фене
    # поэтому работаю в cp866
    cur.execute('SELECT path, name, ext FROM Src WHERE ext = \'.asm\' OR ext = \'.ASM\'')
    files = cur.fetchall()
    id_global = 0   # глобальный id ЛУ (см sdb таб line)
    id = 0          # id ФО
    flag = False    # флаг ФО
    offset = 0  # смещение ФО в файле
    counter = 0 # счетчик строк
    funcs = []  # список ФО
    lines = []  # список ЛУ
    ffl = []    # список связей ФО
    goto = []   # список меток ЛУ
    jmp = []    # список переходов ЛУ
    lll = []    # список связей ЛУ

    for src in files:
        _src = os.path.join(src[0], src[1]+src[2])

        try:
            f = open(_src, "r", encoding='cp866')
            counter = 0
        except:
            print('Open fail [%s]'%_src)
            #tmp = input("Pres any key to continue...")
            continue

        for line in f:
            counter += 1
            if 'proc' in line:# & ('near' in line):
                id += 1
                flag = True
                size = 0
                offset = counter
                name = line[:line.find('proc') -1 ].strip()
            if flag:
                size += 1
            if (flag == True) & ('endp' in line):
                flag = False
                funcs.append([id, name, size, _src, offset, _src, 0])
        f.close()

    for parent in funcs:
        f = open(parent[3], "r", encoding='cp866')
        code = f.read()
        code = code.split("\n")
        f.close()
        #print("\n\n************************************\nFILE=%s\nNAME=%s\nOFFSET=%s\nSIZE=%s\nCODE {" % (parent[3], parent[1], parent[4], parent[2]) )
        body = ""
        for i in range(parent[2]):
            #print("\t%s" % code[ parent[4]+i-1 ])
            body += code[ parent[4] + i - 1 ] + "\n"
        #print("}")
        if level == 2:
            id_global, lines, lll, jmp, goto = line_asm(cur, body, parent[4], parent[0], id_global, lines, lll, jmp, goto)    
        for child in funcs:
            if child[3] == parent[3]:
                if child[0] == parent[0]:
                    continue
                if child[1] in body:
                    ffl.append([parent[0], child[0]])
    if level == 2:
        for jm in jmp:
            for go in goto:
                if jm[1] == go[1]:
                    if jm[2] == go[2]:
                        lll.append([jm[0], go[0], 0, jm[2]])
                    else:
                        lll.append([jm[0], go[0], 0, -1])
    write_db(cur, [funcs, ffl, lines, lll], level)
    return id_global

def func_is_useful(func, lang):
    refs = ''
    if lang == 'C++':
        refs = func.refs('C Callby')
    elif lang == 'Java':
        refs = func.refs('Java Callby')
    elif lang == 'C#':
        refs = func.refs('C# Callby')
    elif lang == 'Pascal':
        refs = func.refs('Pascal Callby')
    elif lang == 'Web':
        refs = func.refs('Web Php Callby, Web Javascript Callby')
        pass

    if len(refs) > 0:
        return True

    return False

def write_function_table(udb, db, cur, lang_list, level, name_pr, config, lid):
    log = open('log_function.txt', 'w')
    temp = open ('temp.txt', 'w')
    log.write('### Function ###\n')
    str_INSERT = 'INSERT OR REPLACE INTO Function (id, name, ret_type, type_param, count_line, def_file, def_offset, dec_file, is_useful, udb_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);'
    id_func = 1
    cur_id = 0
    level2c = 0
    line_done = False
    for lang in lang_list:

        opt_ents = ''
        opt_def = ''
        if lang == 'C++':
            opt_ents = 'C Function ~Unknown ~Unresolved ~Virtual'
            opt_def = 'C Definein'
        elif lang == 'Java':
            opt_ents = 'Java Method ~Unknown ~Unresolved ~Abstract'
            opt_def = 'Java Definein'
        elif lang == 'C#':
            opt_ents = 'C# Method ~Unknown ~Unresolved'
            opt_def = 'C# Definein'
        elif lang == 'Web':
            opt_ents = 'Web Javascript Function ~Unresolved, Web Php Function ~Unresolved ~Abstract'
            opt_def = 'Web Javascript Definein, Web Php Definein'
        elif lang == 'Pascal':
            opt_ents = 'Pascal Procedure ~Unknown ~Unresolved ~Virtual, Pascal Function ~Unknown ~Unresolved'
            opt_def = 'Pascal Definein'
        elif lang == 'Python':
            opt_ents = 'Python Function ~Unresolved'
            opt_def = 'Python Definein'
        elif lang == 'ASM':
            id_line_global = func_asm(cur, name_pr, level)
        else:
            print('С такими языками мы не работаем','Langlist: %s' % lang_list, sep = '\n')
            return

        id_line_global = 0
        for func in udb.ents(opt_ents):
            about_func = []

            #Нас не интересуют методы из стандартной библиотеки а также Get и Set
            if ((lang in ['C#', 'Python']) and ((func.library() == 'Standard') or (func.simplename() in ['Get','Set','get','set']))) or ((lang == 'Java') and (func.library() == 'Standard')):
                continue
            
            #Проверяем последнюю записанную функцию
            if id_func <= lid:
                continue
            else: cur_id = id_func
            
            #Устанавливаем соответствие идентификаторов из нашей базы и из базы understan'a
            about_func.append(id_func)
            id_func += 1
            
            if level == 2:
                tmp = line.line(func, db,  cur, id_line_global, name_pr)
                id_line_global = 0
                id_line_global = tmp
                level2c = tmp
                # рисуем функции
                graph.draw_function(func, name_pr, log)
            about_func.append(func.name())
            about_func.append(func.type())
            about_func.append(func.parameters(False))
            try:
                about_func.append(func.metric(func.metrics())['CountLine'])
            except:
                about_func.append('0')

            ref = func.ref(opt_def)
            try: def_file = ref.ent()
            except: 
                id_func -= 1
                continue

            if lang == 'Java':
                while(def_file.ref('Java Definein') != None):
                    def_file = def_file.ref('Java Definein').ent()
            if lang == 'C#':
                while(def_file.ref('C# Definein') != None):
                    def_file = def_file.ref('C# Definein').ent()
            if lang == 'Pascal':
                while(def_file.ref('Pascal Definein') != None):
                    def_file = def_file.ref('Pascal Definein').ent()
            if lang == 'Web':
                if str(def_file.kind()).find('Interface') != -1:
                    print(def_file.kind())  
                    id_func -= 1              
                    continue
            fi = ref.file()
            about_func.append(fi.longname())
            about_func.append(ref.line())

            if lang == 'C++':
                dec_files = ''
                for ref in func.refs('C Declarein'):
                    dec_files = dec_files + ref.ent().name() + ' => ' + str(ref.line()) + '\n'
                about_func.append(dec_files)
            else:
                about_func.append(fi.longname())

            about_func.append(func_is_useful(func, lang))
            about_func.append(func.id())
            try:
                cur.execute(str_INSERT, tuple(about_func))
            except sqlite3.DatabaseError as err:
                log.write('Ошибка при записи данных в таблицу.\nДанные:\n')
                for i in range(0, len(about_func)):
                    log.write('\t%d) %s\n' % (i + 1, str(about_func[i])))
                log.write('Error: %s\n' % err)
            else:
                db.commit()
    log.write('### Запись завершена ###\n')
    log.close()
    if  level2c != 0:
        line_done = True
    return True, cur_id, line_done, id_line_global