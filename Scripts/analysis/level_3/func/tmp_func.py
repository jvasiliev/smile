import sqlite3
def write_tmp_func(udb, db, cur, lang_list):
    log = open('log_tmp_func.txt', 'w')
    log.write('### TEMP_Func ###\n')
    str_CREATE_func = 'CREATE TEMP TABLE TEMP_Func (id_func_sql INTEGER, id_func_udb INTEGER);'
    str_INSERT_func = 'INSERT INTO TEMP_Func (id_func_sql, id_func_udb) VALUES (?, ?);'
    try:
        cur.execute(str_CREATE_func)
    except sqlite3.DatabaseError as err:
        log.write('Не удалось создать таблицу.\nError: %s\n' % err)
        log.close()
        return
    for lang in lang_list:
        if 'C++' == lang:
            opt_ents = 'C Function ~Unknown ~Unresolved ~Virtual'
        elif 'Java' == lang:
            opt_ents = 'Java Method ~Unknown ~Unresolved ~Abstract'
        elif 'C#' == lang:
            opt_ents = 'C# Method ~Unknown ~Unresolved'
        elif 'Pascal' == lang:
            opt_ents = 'Pascal Procedure ~Unknown ~Unresolved ~Virtual, Pascal Function ~Unknown ~Unresolved'    
        elif 'Web' == lang:
            opt_ents = 'Web Php Function ~Unresolved, Web Javascript Function ~Unresolved'
        elif 'Python' == lang:
            opt_ents = 'Python Function, Python Function ~Unresolved'
        else:
            continue

        log.write('NAME\t::\tID_UDB\t::\tID_SQL\n')
        func_list = udb.ents(opt_ents)
        
        id_sql = 1
        for func in func_list:
            log.write('%s\n'%func)
            if (('C#' == lang) and ((func.library() == 'Standard') or (func.simplename() in ['Get','Set','get','set']))) or ((lang == 'Java') and (func.library() == 'Standard')):
                continue
            try:
                log.write('%s\t::\t%d\t->\t%d\n' % (func.simplename(), func.id(), id_sql))
                cur.execute(str_INSERT_func, [id_sql, func.id()])
            except sqlite3.DatabaseError as err:
                log.write('Ошибка при записи данных.\nДанные:\t%s\n\tid_sql - %d\n\tid_udb - %d\nError: %s\n' % (func.simplename(), id_sql, func.id(), err))
            else:
                db.commit()
                id_sql = id_sql + 1
    log.write('### Запись завершена ###\n')
    log.close()

