﻿import understand
import sqlite3
import os, sys
import configparser

# C/C++,C# Java
from .func import tmp_func, function, func_func_link, perl
#from .din import ins_mark, an_trace

# C/C++
from .pointer import pointer
from .gVar import tmp_gVar, gVar, gVar_func_link


def und_db_open(name):
    print(os.listdir(os.getcwd()))
    log = open("log_main_udb.txt", "w")
    if not os.path.exists(name):
        print("База данных understand для данного проекта не найдена.")
        return None

    try:
        db = understand.open(name)
    except understand.UnderstandError as err:
        log.write("Ошибка при открытии базы данных understand\'a.\n"\
                  "Error: %s" % err)
    else:
        log.close()
        return db

def sql_db_open(name):
   #log = open("log_main_udb.txt", "w")
   #if not os.path.exists(name):
    try:
        db = sqlite3.connect(name)
    except sqlite3.DatabaseError as err:
        log.write("Ошибка при открытии базы данных sqlite.\n"\
                "Error: %s\n" % err)
        return None
 #   else:
 #       log.write("База данных %s создана.\n" % name)
 #       return db
    return db
   #else:
   #    print("База данных sqlite с таким именем уже существует.")
   #    return None
   #log.close()


def create_tables(cur, lang_list):

    log = open("er_main_udb.log", "w+")
    str_CREATE = '''\
    CREATE TABLE IF NOT EXISTS Function (id INTEGER PRIMARY KEY, name TEXT, ret_type TEXT, type_param TEXT, count_line INTEGER, def_file TEXT, def_offset INTEGER, dec_file TEXT, is_useful BOOL, udb_id INTEGER);
    CREATE TABLE IF NOT EXISTS Func_Func_Link (id_parent INGETER, id_child INTEGER);
    CREATE TABLE IF NOT EXISTS Line (id_func INTEGER, id_line_in_func INTEGER, id_global INTEGER, begin INTEGER,size INTEGER, is_useful INTEGER);
    CREATE TABLE IF NOT EXISTS Line_Line (id_line1 INTEGER, id_line2 INTEGER,flag_return INTEGER, id_func INTEGER);
    CREATE TABLE IF NOT EXISTS gVar (id INTEGER PRIMARY KEY, name TEXT, type TEXT, def_file TEXT, def_offset TEXT);
    CREATE TABLE IF NOT EXISTS gVar_Func_Link (id_gVar INTEGER, id_func INTEGER);
    '''

    if 'C++' in lang_list:
        str_CREATE = str_CREATE + '''\
        CREATE TABLE IF NOT EXISTS Pointer (id_func_1 INTEGER, id_func_2 INTEGER);
        '''
    try:
        cur.executescript(str_CREATE)
    except sqlite3.DatabaseError as err:
        print("Ошибка при создании таблиц")
        log.write("Error: %s\n" % err)
        log.close()
        return False
    return True

def write_conf(path, name, configf):
    with open(os.path.join(path, name + ".conf"), "w") as config:
        configf.write(config)

def main_udb(name_pr, path_pr, lang_list, level, conf):
    
    udb = und_db_open(path_pr+"/Databases/"+name_pr+".udb")
    if udb == None: return False

    sdb = sql_db_open(path_pr+"/Databases/"+name_pr+".sdb")
    if sdb == None: return False

    scur = sdb.cursor()

    if not create_tables(scur, lang_list): return False

# perl kostil
    if 'Perl' in lang_list:
        perl.perl_func(name_pr, path_pr)
        return True

    tmp_func.write_tmp_func(udb, sdb, scur, lang_list)

# эти таблицы заполняются не зависимо от языка

    try:
        func_done = conf.getboolean('third_level', 'func')
        if func_done == False: raise configparser.NoOptionError('third_level', 'func')
        #last_fid = conf.getint('third_level', 'func_id')
        print('======= Поиск функциональных объектов был выполнен ранее ======')
        line_done = conf.getboolean('second_level','line')
        print(line_done)
        if line_done == False: raise configparser.NoOptionError('second_level', 'line')
        print('======= Поиск линейных участков был выполнен ранее ======')
    except configparser.NoOptionError as err:
        lin = 0
        last_fid = conf.getint('third_level', 'func_id')
        last_lfid = conf.getint('second_level', 'lf_for_line')
        if (last_fid == 0) and (last_lfid == 0) and (level == 3):
            print('======= Начат поиск функциональных объектов ======')
        elif (last_fid == 0) and (last_lfid == 0) and (level == 2):
            print('======= Начат поиск функциональных объектов и линейных участков ======')
        elif (last_lfid == 0) and (last_fid != 0):
            lin = 1
            last_fid = 0
            print('======= Начат поиск линейных участков ======')
        else: print('======= Продолжение поиска функциональных объектов или линейных участков ======')
        func_done, last_fid, line_done, last_lfid = function.write_function_table(udb, sdb, scur, lang_list, level, name_pr, conf, last_fid)
        conf.set('third_level', 'func', str(func_done))
        conf.set('third_level', 'func_id', str(last_fid))
        conf.set('second_level', 'line', str(line_done))
        conf.set('second_level', 'lf_for_line', str(last_lfid))
        write_conf(path_pr, name_pr, conf)
        if (last_fid != 0) and (last_lfid != 0) and (lin == 0):
            print('======= Поиск функциональных объектов и линейных участков завершен ======')
        elif (last_fid != 0) and (last_lfid == 0):
            print('======= Поиск функциональных объектов завершен ======')
        elif (last_fid != 0) and (lin == 1):
            print('======= Поиск линейных участков завершен ======')
        else: print('======= Поиск завершен ======')
    
    try:
        ff_done = conf.getboolean('third_level', 'func_func')
        if ff_done == False: raise configparser.NoOptionError('third_level', 'func_func')
        #last_fid_fl = conf.getint('third_level', 'LfId_for_linking')
        print('======= Поиск связей функциональных объектов был выполнен ранее ======')
    except configparser.NoOptionError as err:
        last_fid_fl = conf.getint('third_level', 'LfId_for_linking')
        if last_fid_fl == 0:
            print('======= Начат поиск связей функциональных объектов ======')
        else: print('======= Продолжение поиска связей функциональных объектов ======')
        ff_done, last_fid_fl = func_func_link.write_func_func_link_table(udb, sdb, scur, lang_list, last_fid_fl)        
        conf.set('third_level', 'func_func', str(ff_done))
        conf.set('third_level', 'LfId_for_linking', str(last_fid_fl))
        write_conf(path_pr, name_pr, conf)
        print('======= Поиск связей функциональных объектов завершен =======')
    
    tmp_gVar.write_tmp_gVar(udb, sdb, scur, lang_list)
    
    try:
        gVar_done = conf.getboolean('third_level', 'gVar')
        if gVar_done == False: raise configparser.NoOptionError('third_level', 'gVar')
        #last_gVar_id = conf.getint('third_level', 'gVar_id')
        print('======= Поиск информационных объектов был выполнен ранее ======')
    except configparser.NoOptionError as err:
        last_gVar_id = conf.getint('third_level', 'gVar_id')
        if last_gVar_id == 0:
            print('======= Начат поиск информационных объектов =======')
        else: print('======= Продолжение поиска информационных объектов ======')
        gVar_done, last_gVar_id = gVar.write_gVar_table(udb, sdb, scur, lang_list, last_gVar_id)
        conf.set('third_level', 'gVar', str(gVar_done))
        conf.set('third_level', 'gVar_id', str(last_gVar_id))
        write_conf(path_pr, name_pr, conf)
        print('======= Поиск информационных объектов завершен =======')
    
    try:
        gVar_func_done = conf.getboolean('third_level', 'gVar_func')
        if gVar_func_done == False: raise configparser.NoOptionError('third_level', 'gVar_func')
        #last_gVar_f_id = conf.getint('third_level', 'LgId_for_linking')
        print('======= Поиск связей информационных объектов был выполнен ранее ======')
    except configparser.NoOptionError as err:
        last_gVar_f_id = conf.getint('third_level', 'LgId_for_linking')
        if last_gVar_f_id == 0:
            print('======= Начат поиск связей информационных объектов =======')
        else: print('======= Продолжение поиска связей информационных объектов ======')
        gVar_func_done, last_gVar_f_id = gVar_func_link.write_gVar_func_link_table(udb, sdb, scur, lang_list, last_gVar_f_id)
        conf.set('third_level', 'gVar_func', str(gVar_func_done))
        conf.set('third_level', 'LgId_for_linking', str(last_gVar_f_id))
        write_conf(path_pr, name_pr, conf)
        print('======= Поиск связей информационных объектов завершен =======')
        
# заполняются только для языка C++
    if 'C++' in lang_list:
        pointer.write_pointer_table(udb, sdb, scur)

# закрываем все соединения
    scur.close()
    sdb.close()
    udb.close()
    return True