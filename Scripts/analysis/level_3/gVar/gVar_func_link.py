﻿import sqlite3
import os
def write_gVar_func_link_table(udb, sql_con, sql_cur, lang_list, lid):
    log = open('log_gVar_func_link.txt', 'w')
    log.write('### gVar_Fucn_Link ###\n')
    log2 = open('log_gfl.txt', 'w')
    cur_id = 0

    str_FIND_func = 'SELECT id, udb_id FROM Function;'#
    str_FIND_gVar = 'SELECT id_gVar_sql FROM TEMP_gVar WHERE id_gVar_udb=%d;'
    str_INSERT = 'INSERT OR REPLACE INTO gVar_Func_Link (id_gVar, id_func) VALUES (%d, %d);'

    try:
        sql_cur.execute(str_FIND_func)
    except sqlite3.DatabaseError as err:
        log.write('Function table.\nError: %s\n' % err);
        log.close()
        return
    else:
        list_id_func = sql_cur.fetchall()

    for lang in lang_list:

        if lang == 'Java':
            entity = udb.ents('Java Public Variable Member')
            name_ref = 'Java Modifyby Init'
        elif lang == 'C++':
            entity = udb.ents('C Global Object')
            name_ref = 'C Setby, C Useby, C Modifyby'
        elif lang == 'C#':
            entity = udb.ents('C# Public Field Member')
            name_ref = 'C# Modifyby Init'
        elif lang == 'Pascal':
            entity = udb.ents('Pascal Global Variable')
            name_ref = 'Pascal Modifyby'
        elif lang == 'Web':
            entity = udb.ents('Web Javascript Variable Global, Web Php Variable Global')
            name_ref = 'Web Javascript Modifyby, Web Php Modifyby'   
        elif lang == 'Python':
            entity =  udb.ents('Python Variable Global')
            name_ref = 'Python Modifyby, Python Useby, Python Setby, Python Modifyby Python Init'
        for gVar in entity:
            try:
                log2.write('%s \n' % gVar)
            except: log2.write('%s \n' % "BADNAME_GVAR")
            if (lang in ['C#', 'Java', 'Python']) and (gVar.library() == 'Standard'):
                log2.write('continue \n')
                continue  
            try:
                #print(str_FIND_gVar % gVar.id())
                sql_cur.execute(str_FIND_gVar % gVar.id())
            except sqlite3.DatabaseError as err:
                log.write('Error: %s\n' % (err))
                continue
            else:
                id_gVar_sql = sql_cur.fetchone()[0]
                if id_gVar_sql <= lid:
                    continue
                else: cur_id = id_gVar_sql
            refs = gVar.refs(name_ref)

            if lang == 'C++':
                for id_func in list_id_func:
                    for ref in refs:
                        if ref.ent().id() == id_func[1]:
                            print(ref.ent().name())
                            log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                            try:
                                sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                            except sqlite3.DatabaseError as err:
                                log.write('Error: %s\n' % (err));
                            else:
                                sql_con.commit()
                                break
                                
            elif lang == 'Java':
                try:
                    gVar_sp = gVar.name().split('.',1)
                    for id_func in list_id_func:
                        for ref in refs:
                            if ((ref.kind().name() == 'Set') or (ref.kind().name() == 'Use')):
                                    ref_sp = ref.ent().name().split('.',1)
                                    if gVar_sp[0] != ref_sp[1]:
                                        if ref.ent().id() == id_func[1]:
                                            log.write(ref.ent().name())
                                            log.write('\n')
                                            log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                                            try:
                                                
                                                sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                                            except sqlite3.DatabaseError as err:
                                                log.write('Error: %s\n' % (err));
                                            else:
                                                sql_con.commit()
                                                break
                except:
                    log.write('Error in split or something...\n');
            elif lang == 'C#':
                print('C#')
                if (gVar.library() != 'Standard'):
                    for id_func in list_id_func:
                        for ref in refs:
                            kin_sp = ref.ent().kind().name().split(' ',1)
                            if (ref.kind().name() in ['Set', 'Setby', 'Use', 'Useby', 'Get']) and ((kin_sp[0] == 'Public') or (kin_sp[0] == 'Internal')):
                                if ref.ent().id() == id_func[1]:
                                    log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                                    try:
                                        
                                        sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                                    except sqlite3.DatabaseError as err:
                                        log.write('Error: %s\n' % (err));
                                    else:
                                        sql_con.commit()
                                        break
            elif lang == 'Pascal':
                print('Pascal')
                for id_func in list_id_func:
                    for ref in refs:
                        kin_sp = ref.ent().kind().name().split(' ',1)
                        if (ref.kind().name() in ['Set', 'Setby', 'Use', 'Useby', 'Get']) and ((kin_sp[0] == 'Procedure') or (kin_sp[0] == 'Function')):
                            if ref.ent().id() == id_func[1]:
                                log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                                try:
                                    sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                                except sqlite3.DatabaseError as err:
                                    log.write('Error: %s\n' % (err));
                                else:
                                    sql_con.commit()
                                    break
            elif lang == 'Web':
                print('Web')
                for id_func in list_id_func:
                    for ref in refs:
                        # Проверка на NoneType
                        try:
                            ref.ent()
                        except: continue
                        if ref.ent().id() == id_func[1]:
                            log.write(ref.ent().name())
                            log.write('\n')
                            log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                            print(ref.ent().name())
                            try:
                                sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                            except sqlite3.DatabaseError as err:
                                log.write('Error: %s\n' % (err));
                            else:
                                sql_con.commit()
                                break
            elif lang == 'Python':
                print('Python')
                for id_func in list_id_func:
                    for ref in refs:
                        # Проверка на NoneType
                        try:
                            ref.ent()
                        except: continue
                        if ref.ent().id() == id_func[1]:
                            log.write(ref.ent().name())
                            log.write('\n')
                            log2.write('%d \t %d \n' % (id_gVar_sql, id_func[0]))
                            print(ref.ent().name())
                            try:
                                sql_cur.execute(str_INSERT % (id_gVar_sql, id_func[0]))
                            except sqlite3.DatabaseError as err:
                                log.write('Error: %s\n' % (err));
                            else:
                                sql_con.commit()
                                break                       
            else:
                print('No language')
                return

    log.write('### gVar_func_link ###\n')
    log.close()
    log2.close()
    return True, cur_id