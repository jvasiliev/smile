﻿import sqlite3
def write_gVar_table(udb, sql_con, sql_cur,lang_list, lid):
    log = open('gVar.txt', 'w')
    log.write('### gVar ###\n')


    str_FIND_gVar = 'SELECT id_gVar_sql FROM TEMP_gVar WHERE id_gVar_udb=%d;'
    str_INSERT = 'INSERT OR REPLACE INTO gVar (id, name, type, def_file, def_offset) VALUES (%d, \'%s\', \'%s\', \'%s\', %d);'
    counter = 0
    cur_id = 0
    for lang in lang_list:
        counter += 1
        print(lang, counter, '/', len(lang_list))
        name_ref = '%s Definein' % lang

        if lang == 'Java':
            entity = udb.ents('Java Public Variable Member')
        elif lang == 'C++':
            entity = udb.ents('C Global Object')
        elif lang == 'C#':
            entity = udb.ents('C# Public Field Member')
        elif lang == 'Pascal':
            entity = udb.ents('Pascal Global Variable')
        elif lang == 'Web':
            entity = udb.ents('Web Php Variable Global, Web Javascript Variable Global') 
            name_ref = 'Web Javascript Definein, Web Php Definein'    
        elif lang == 'Python':
            entity =  udb.ents('Python Variable Global')
        else:
            pass
         
        for gVar in entity:
            about_gVar = []        
            if (gVar.library() == 'Standard') and (lang in ['C#', 'Java', 'Python']):
                continue
            try:
                sql_cur.execute(str_FIND_gVar % gVar.id())
            except sqlite3.DatabaseError as err:
                log.write('Ошибка в процессе поиска id_gVar_sql для %s.\nError: %s\n' % (gVar.simplename(), err))
            else:
                try:
                    about_gVar.append(sql_cur.fetchone()[0])
                    if about_gVar[0] <= lid:
                        continue
                    else: cur_id = about_gVar[0]
                except TypeError as err:
                    continue
            about_gVar.append(gVar.simplename())
            about_gVar.append(gVar.type())
            ref = gVar.ref(name_ref)
            
            # Некоторые глобальные переменные в Web нигде не определяются 
            # (Definein возвращает NoneType). Пока скипаем.
            try: def_file = ref.file()
            except: 
                counter -= 1
                log.write('Не найдено отношение Definein для %s. \n' % gVar.simplename())
                continue

            about_gVar.append(def_file.longname())
            about_gVar.append(ref.line())
            try:
                sql_cur.execute(str_INSERT % tuple(about_gVar))
                log.write(str_INSERT % tuple(about_gVar))
            except sqlite3.DatabaseError as err:
                log.write('Ошибка при записи данных в таблицу.\nДанные:\n')
                for i in range(0, len(about_gVar)):
                    log.write('\t%d) %s\n' % (i + 1, str(about_gVar[i])))
                log.write('Error: %s\n' % err)
            except: 
                print("[WARNING] Возможно есть gVar с плохим именем [WARNING] ")                
            else:
                sql_con.commit()

    log.write('### Запись завершена ###')
    log.close()
    return True, cur_id
