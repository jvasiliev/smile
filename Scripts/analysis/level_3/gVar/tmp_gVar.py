﻿import sqlite3
def write_tmp_gVar(udb, db, cur, lang_list):
    log = open('log_temp_gVar.txt', 'w')
    log.write('### TEMP_gVar ###\n')

    str_CREATE_gVar = 'CREATE TABLE IF NOT EXISTS TEMP_gVar (id_gVar_sql INTEGER, id_gVar_udb INTEGER);'
    str_INSERT_gVar = 'INSERT OR REPLACE INTO TEMP_gVar (id_gVar_sql, id_gVar_udb) VALUES (%d, %d);'

    try:
        cur.execute(str_CREATE_gVar)
    except sqlite3.DatabaseError as err:
        log.write('Не удалось создать таблицу.\nError: %s\n' % err)
        log.close()
        return
    id_sql = 1
    for lang in lang_list:

        if lang == 'C++':
            gVar_list = udb.ents('C Global Object')
        elif lang == 'Java':
            gVar_list = udb.ents('Java Public Variable Member')
        elif lang == 'C#':
            gVar_list = udb.ents('C# Public Field Member')
        elif lang == 'Pascal':
            gVar_list = udb.ents('Pascal Global Variable')
        elif lang == 'Web':
            gVar_list = udb.ents('Web Javascript Variable Global, Web Php Variable Global')     
        elif lang == 'Python':
            gVar_list =  udb.ents('Python Variable Global')    
        else:
            pass


        log.write('NAME\t:\tID_UDB\t:\tID_SQL\n')
        for gVar in gVar_list:
            if (lang in ['C#', 'Java', 'Python']) and (gVar.library() == 'Standard'):
                continue
            try:
                log.write('%s\t::\t%d -> %d\n' % (str(gVar.simplename()), gVar.id(), id_sql))
                cur.execute(str_INSERT_gVar % (id_sql, gVar.id()))
            except sqlite3.DatabaseError as err:
                log.write('Ошибка при записи данных в таблицу.\n\
                Данные:\t%s\n\tid_sql - %d\n\tid_udb - %d\n\
                Error: %s' % (gVar.simplename(), id_sql, gVar.id(), err))
            except:
                log.write('%s\t::\t%d -> %d\n' % ("NONAME", gVar.id(), id_sql))
                cur.execute(str_INSERT_gVar % (id_sql, gVar.id()))
            else:
                db.commit()
                id_sql = id_sql + 1


    log.write('### Запись завершена ###\n')
    log.close()
