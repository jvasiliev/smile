import argparse
import sys
import os
import hashlib
import re
import subprocess
import datetime
import configparser

def c_date(filename):
    t = os.path.getctime(filename)
    date = str(datetime.datetime.fromtimestamp(t))
    out_d = date.rsplit(':',1)
    return out_d[0]

# def argParse():
    # parser = argparse.ArgumentParser(description = 'Подсчет контольных сумм')
    # parser.add_argument('-hf','--hash',type = str, default = 'md5', help = 'Хеш функция для подсчета контрольных сумм')
    # parser.add_argument('-i','--input',type = str, default = '.', help = 'Путь к директории')
    # parser.add_argument('-o','--output',type = str, default = '.', help = 'Путь, куда поместить отчет')
    # parser.add_argument('-t','--type',type = str, default = 'html', help = 'Тип выходнго файла')
    # return vars(parser.parse_args())

def getsum(filename,hash_name):
    if hash_name == 'md5':
        m = hashlib.md5()
    if hash_name == 'sha1':
        m = hashlib.sha1()
    if hash_name == 'sha256':
        m = hashlib.sha256()
    fd = open(filename, 'rb')
    b = fd.read()
    m.update(b)
    fd.close()
    return m.hexdigest()

def gost34_11_94(filename):
    """call gost Russian GOST - 34.11.1994"""
    proc = subprocess.Popen('openssl dgst -hex -md_gost94 %s'%(filename),
            shell = True,
            stdout = subprocess.PIPE,
            stdin = subprocess.PIPE,
            stderr = subprocess.PIPE)
    stdout_value,stderr_value = proc.communicate()
    proc.stdin.close()
    return ((str(stdout_value)[15+len(filename):-3]))

def streebog(filename,mode):
    """call streebog Russian GOST - 34.11-2012"""
    command = './gost3411-2012 %s'
    if mode == 2:
        command = './gost3411-2012 -2 %s'
    proc = subprocess.Popen(command%(filename),
            shell = True,
            stdout = subprocess.PIPE,
            stdin = subprocess.PIPE,
            stderr = subprocess.PIPE)
    stdout_value,stderr_value = proc.communicate()
    proc.stdin.close()
    return(str(stdout_value)[25+len(filename):-3])    
    
def get_bytes(path, data, end):
    bytes = 0
    temp_data = data[3]
    if end != True:
        bytes = os.path.getsize(path)
        data[2] += int(bytes)
    else:
        data[3] = data[2]
        bytes = data[2] - temp_data
    return bytes, data

def c_lines(file_path):
    lines = 0
    #Новая версия поиска строк в файле (основанная на поиске в бинарном потоке символа конца строки)
    with open(file_path, 'rb') as binf:
        while True:
            binbody = binf.read()
            if not binbody: break
            for ch in binbody:
                if chr(ch) == '\n':
                    lines += 1
    #Старая версия поиска строк (основанная на считывании строк из файла)
    # encodings = ['utf-8', 'windows-1251', 'windows-1252', 'us-ascii', 'iso8859-1', 'iso-8859-5', 'utf16', 'koi8_r']
    # for e in encodings:
        # try:
            # scan = open(file_path, 'r', encoding=e)
            # lines = len(scan.readlines())
            # break
        # except: lines = 0
        # finally: scan.close()
    return lines
    
def get_lines(file_path, data, end):
    count_lines = 0
    temp_data = data[5]
    if end != True:
        count_lines = c_lines(file_path)
        data[4] += count_lines 
    else:
        data[5] = data[4]
        count_lines = data[4] - temp_data
    return count_lines, data 
    
def get_id(data,total):
    num = data[0]
    count = data[1]
    if total != True:        
        num += 1
        data[0] = num
    else:
        data[1] = num
        num = num - count
    return num, data
    
# подсчет суммарного количества файлов в заданном пути
def total_count(inp_path, path_out, data):
    cur_path = os.path.join(path_out,'result.html')
    
    cur_id_all, data = get_id(data,True)
    cur_bytes_all, data = get_bytes(inp_path, data, True)
    cur_lines_all, data = get_lines(inp_path, data, True)
    
    id_all, data = get_id(data,False)
    bytes_all, data = get_bytes(inp_path, data, True)
    lines_all, data = get_lines(inp_path, data, True)
    
    id_all_total = id_all - 1
    try: curfile = open(cur_path, "r+")
    except: 
        curfile.close()
        return data
    curfile.seek(curfile.seek(0,2)-26)
    total = []
    total.append('<tr class=even>\n<td colspan=3 align=center>Итого: %d</td>\n<td align=center>%d</td>\n<td align=center>%d</td>\n<td align=center>-</td>\n</tr>\n' % (cur_id_all, cur_bytes_all, cur_lines_all))
    total.append('<tr class=group-1>\n<td colspan=3 align=center>Всего файлов: %d</td>\n<td align=center>%d</td>\n<td align=center>%d</td>\n<td align=center>-</td>\n</tr>\n</table>\n</body>\n</html>' % (id_all_total, data[2], data[4]))
    result = ''
    for i in total:
        result += i
    curfile.write(result)
    curfile.close()
    data[6] = 1
    return data
    
def write_file(path_inp,file_inp,path_out,sum,f_type,s_type,flag, data):
    inp_path = os.path.join(path_inp,file_inp)    
    if f_type == 'txt':
        filename = 'crc.txt'
        try:
            with open(os.path.join(path_out,filename), "a", encoding = "utf-8") as nfile:
                nfile.write('%s;%s;%s\n' % (os.path.join(path_inp,file_inp), str(os.path.getsize(inp_path)), str(sum)))
        except:
            print('File exception in txt!')
    elif f_type == 'html':
        filename = 'result.html'
        cur_path = os.path.join(path_out,filename)
        if os.path.isfile(cur_path):            
            if flag:
                if (data[6] == 0):
                    tot_id, data = get_id(data,True)
                    tot_bytes, data = get_bytes(inp_path, data, True)
                    tot_lines, data = get_lines(inp_path,data, True)
                    summary = '<tr class=even>\n<td colspan=3 align=center>Итого: %d</td>\n<td align=center>%d</td>\n\n<td align=center>%d</td>\n\n<td align=center>-</td>\n</tr>' % (tot_id, tot_bytes, tot_lines)
                    directory = '\n<tr class=group-1>\n<td colspan=6 align=center>Каталог: %s</td>\n</tr>' % (path_inp)
                else:
                    summary = '\n</table><h1>Дополнение: </h1><table cellspacing="4" cellpadding="0" width="100%" style="border-collapse:collapse">\n<tr class=heady>\n<th class=num><b>№</b></th>\n<th class=name><b>Имя файла</b></th>\n<th class=name><b>Дата создания</b></th>\n<th class=count><b>Длина файла, байт</b></th>\n<th class=lines><b>Длина файла, строк</b></th>\n<th class=contrc><b>Контрольная сумма</b></th>\n</tr>\n'
                    directory = '\n<tr class=group-1>\n<td colspan=6 align=center>Каталог: %s</td>\n</tr>' % (path_inp)
                    data[6] = 0
            else: 
                directory = ''
                summary = ''
            
            cur_id, data = get_id(data, False)
            cur_bytes, data = get_bytes(inp_path, data, False)
            cur_lines, data = get_lines(inp_path, data, False)
            
            nr_pack = []
            nr_pack.append(summary)
            nr_pack.append(directory)
            nr_pack.append('<tr class=even>\n<td align=center>%d</td>\n<td align=center>%s</td>\n<td align=center>%s</td>\n<td align=center>%d</td>\n<td align=center>%d</td>\n' % (cur_id, str(file_inp), c_date(inp_path), cur_bytes, cur_lines))
            nr_pack.append('<td align=center>%s</td>\n</tr>\n</table>\n</body>\n</html>' % (str(sum)))
            new_row = ''
            for i in nr_pack:
                new_row += i
            curfile = open(os.path.join(path_out,filename), "r+")
            curfile.seek(curfile.seek(0,2)-26)
            curfile.write(new_row)
            curfile.close()
        else:
            count_lines = c_lines(inp_path)
            data[2] = os.path.getsize(inp_path)
            data[4] = count_lines
            html_pack = []
            style = ''
            html_pack.append('<!DOCTYPE HTML>\n<html>\n<head>\n<meta http-equiv="content-type" content="text/html; charset=windows-1251">')
            html_pack.append('''\n<style>\n
            BODY
            {
                font-family:verdana;
            }

            TABLE
            {
                border: 0;
                cellspacing: 0;
                cellpadding: 0;
                table-layout: fixed; 
                width: 900px;  
                word-wrap:break-word;
                border-collapse: collapse;
                
            }

            TR.nav
            {
                font-weight:bold;
                font-size: 12px;
            }

            TR.king {

                font: 10pt "Verdana";
                font-weight: bold;
                
            }
            TR.group-1 {
                font: 9pt "Verdana";
                font-weight: bold;
            }
            TR.group-2 {
                font-weight: bold;
            }
            TR.group-3 {
                font-weight: bold;
            }
            /* общее для столбцов */
            TD, TH {
                padding: 5px;
            }
            TH
            {
                align:left;
            }

            /* Столбцы */

            th.id_src { width: 37px; }
            th.id_bin { width: 37px; }
            th.id { width: 37px; }
            th.fullname { width: 50% ;}
            th.is_useful { width: 50 px;}
            th.is_marked { width: 50 px;}
            th.len { width: 100 px; }
            th.crc { width: 100 px; }
            th.num {width: 35px; }
            th.glob {width: 30px; }
            th.name{width: 150px ;}
            th.source{ width: 50% ;}
            th.parameters{ width: 150px; }
            th.ret_type {width: 50px; }
            th.lines {width: 50px; }
            th.count {width: 65px; }
            th.contrc {width: 115px; }
            th.offset {width: 50px; }
            th.called {align: left; width:50px }
            th.calling {align: left; width:50px }
            th.percent {width: 80px; text-align: right;}

            td.pad {width: 15px; }
            td.percent {text-align: right;}
            td.param_name {width: 190px; }
            td.param_value {width: 500px; }
            td.param_count {width: 50px; }
            td.param_id {width: 50 px; }
            td.param2_name {width: 190px; }
            td.param2_value {width: 500px; }
            td.param2_count {width: 50px; }
            td.param2_id {width: 50 px; }
            td.param3_name {width: 190px; }
            td.param3_value {width: 500px; }
            td.param3_count {width: 50px; }
            td.param3_id {width: 50 px; }
            td.chck {text-align: center;}

                .barcontainer {
                        padding: 2px;
                            padding-right: 8px;
                        width: 100px;
                        }

                .bar {
                    background-color: #DDD;
                    background-color: rgba(0, 0, 0, 0.1);
                    border-radius: 8px 8px 8px 8px;
                    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.5);
                    height: 7px;
                    padding: 1px;
                    position: relative;
                    width: 100%;
                }

                .bar > span {

                    background-color: #AEDB4B;
                    border-radius: 6px 6px 6px 6px;
                    box-shadow: 0 -2px 2px rgba(0, 0, 0, 0.2) inset, 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
                    display: block;
                    height: 100%;
                }

                .bar > span:before {
                    position: absolute;
                    width: 25%;
                    height: 100%;
                    left: 0;
                    top: 0;
                    border-right: 1px solid rgba(0,0,0,0.1);
                    content: ' ';
                }
                .bar > span:after {
                    position: absolute;
                    width: 25%;
                    height: 100%;
                    left: 50%;
                    top: 0;
                    border-left: 1px solid rgba(0,0,0,0.1);
                    border-right: 1px solid rgba(0,0,0,0.1);
                    content: ' ';
                }

                .check
                {
                    width: 50px;
                    padding-left: 30%;
                }
            /* общее для строк*/
            TR
            {
                font: 8pt "Arial Cyr";
            }
            /* четные строки */


            TR.heady
            {
                background: #758691;
                color: #ffffff;
            }

            TR.even
            {
                background: #ffffff;
            }
            /* нечетные строки */
            TR.odd
            {
                background: #e7e8e9;
            } 
            /* избыточные файлы */
            TR.is_not_useful
            {
              opacity:0.65;
            filter:alpha(opacity=65);
            -moz-opacity:0.65;
            -khtml-opacity:0.65;
            }
            TR.group-2
            {
                background: #CFD0D2;
            }

            TR.group-3
            {
                background: #C1C7C6;
            }

            a:link { text-decoration : none; color : #3366cc; border: 0px;}
            a:active { text-decoration : none; color : #3366cc; border: 0px;}
            a:visited { text-decoration : none; color : #7690A3; border: 0px;}
            a:hover { text-decoration : none; color : #ff5a00; border: 0px;}


            tr td:first-child, th:first-child {
                border-top-left-radius: 5px;
                border-bottom-left-radius: 5px;

            }

            tr td:last-child, th:last-child {
                border-top-right-radius: 5px;
                border-bottom-right-radius: 5px;
            }

            TD.col1
            {
                width: 10px;
            }
            TD.col2
            {
                width: 15 px;
            }

                .chuck {
                    background-color: #DDD;
                    background-color: rgba(0, 0, 0, 0.1);
                    text-align: center;
                    background-position: center;
                    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.5);
                    height: 15px;
                    margin-left: 40%;
                    position: relative;
                    width: 15px;
                }

                .chuck > span {
                    background-color: #AEDB4B;
                    box-shadow: 0 -2px 2px rgba(0, 0, 0, 0.2) inset, 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
                    display: block;
                    height: 100%; 
                    width: 100%;
                    text-align: center;
                }

            </style>\n\n<title>Отчет</title>\n</head>''')
            html_pack.append('\n<body>\n<h1>ОТЧЁТ о фиксации исходного состояния</h1>\n')
            html_pack.append('<h1>_____________________________________</h1>\n')
            html_pack.append('<h1>(%s)</h1>\n' % (s_type))
            html_pack.append('<h3><a href="root.html">К списку отчетов</a> | <a href="../../index.html"> На главную </a> | <a href="javascript:history.back()" onMouseOver="window.status="Назад";return true">Назад</a></h3>')
            html_pack.append('<table cellspacing="4" cellpadding="0" width="100%" style="border-collapse:collapse">\n')
            html_pack.append('<tr class=heady>\n<th class=num><b>№</b></th>\n<th class=name><b>Имя файла</b></th>\n<th class=name><b>Дата создания</b></th>\n<th class=count><b>Длина файла, байт</b></th>\n')
            html_pack.append('<th class=lines><b>Длина файла, строк</b></th>\n<th class=contrc><b>Контрольная сумма</b></th>\n</tr>\n')
            html_pack.append('<tr class=group-1>\n<td colspan=6 align=center>Каталог: %s</td>\n</tr>\n' % (path_inp))
            html_pack.append('<tr class=even>\n<td align=center>%d</td>\n<td align=center>%s</td>\n<td align=center>%s</td>\n<td align=center>%d</td>\n<td align=center>%d</td>\n' % (1,str(file_inp), c_date(inp_path), os.path.getsize(inp_path), count_lines))
            html_pack.append('<td align=center>%s</td>\n</tr>\n</table>\n</body>\n</html>' % (str(sum)))            
            result = ''
            for i in html_pack:
                result += i
            nfile = open(cur_path, 'w')
            nfile.write(result)
            nfile.close()
    else: print('Error in extension')
    return False, data

def ext_table(out_path, cur_ext):
    count = 1
    html_ext = []
    html_ext.append('\n</table><br><table cellspacing="4" cellpadding="0" style="border-collapse:collapse">\n')
    html_ext.append('<h1>Отчёт о расширениях файлов</h1>\n')
    html_ext.append('<tr class=heady>\n<th class=num align=center><b>№ пп</b></td>\n')
    html_ext.append('<th class=lines align=center><b>Расширение</b></td>\n')
    html_ext.append('<th class=lines align=center><b>Кол-во файлов</b></td>\n')
    html_ext.append('<th class=lines align=center><b>Общая длина, байт</b></td>\n')
    html_ext.append('<th class=lines align=center><b>Общая длина, строк</b></td>\n</tr>')    
    for mass in cur_ext:
        html_ext.append('<tr class=even>\n<td align=center>%d</td>\n<td align=center>%s</td>\n<td align=center>%s</td>\n<td align=center>%s</td>\n<td align=center>%s</td>\n</tr>\n' % (count, str(mass[0]), str(mass[1]), str(mass[2]), str(mass[3])))
        count += 1
    html_ext.append('</table>\n</body>\n</html>')
    res = ''
    for j in html_ext:
        res += j
    curfile = open(os.path.join(out_path,'result.html'), "r+")
    curfile.seek(curfile.seek(0,2)-26)
    curfile.write(res)
    curfile.close()
    
def load_conf(path):
    data = []
    confi = configparser.SafeConfigParser()
    confi.read(os.path.join(path,'html.conf'))
    try:
        file_list_str = confi.get('common', 'files')
        file_list = file_list_str.split(' ')
        data_str = confi.get('common', 'data')
        data_list = re.findall('(\d+)', data_str)
        for i in data_list:
           data.append(int(i))
        cur_ext = []
        str_sec = confi.get('common', 'sect_names')
        list_sec = str_sec.split(' ')
        tmp = ''
        tmp_list = []
        for sections in list_sec:
            for option in confi[sections].values():
                tmp += ' %s' % (str(option))
            tmp_list = tmp.split()
            tmp = ''
            cur_ext.append(tmp_list)
    except (configparser.NoOptionError, configparser.NoSectionError) as err:
        print('Конфигурационный файл был изменен.', 'Error: %s' % err, sep = '\n')
    return data, cur_ext, file_list

def save_conf(path, data, cur_ext, wr_files):
    out_d = ''
    for i in data:
        out_d += ' %s' % (str(i))
    confi = configparser.SafeConfigParser()
    confi.add_section('common')
    confi.set('common', 'data', out_d)
    confi.set('common', 'files', ' '.join(wr_files))
    tot_l_ext = []
    for list_s in cur_ext:
        sec_name = 'sc_%s' % (list_s[0])
        try:
            confi.add_section(sec_name)
        except: pass
        confi.set(sec_name, 'name', str(list_s[0]))
        confi.set(sec_name, 'count', str(list_s[1]))
        confi.set(sec_name, 'byte_len', str(list_s[2]))
        confi.set(sec_name, 'str_len', str(list_s[3]))
        tot_l_ext.append(sec_name)
    confi.set('common', 'sect_names',' '.join(tot_l_ext))
    with open(os.path.join(path,'html.conf'), 'w+') as config:
        confi.write(config)
   
def check_cs_start(input_path, otput_path, out_file_type, crc_type):
    #argv = argParse()
    cur_dir = ''
    data = [1,0,0,0,0,0,0]    
    ext_check = []
    cur_ext = []
    cur_ext_data = []
    cur_files = []
    
    #otput_path - argv['output']
    #input_path - argv['input']
    #out_file_type - argv['type']
    #crc_type - argv['hash']
    
    if os.path.isfile(os.path.join(otput_path,'html.conf')):
        data, cur_ext, cur_files = load_conf(otput_path)
        for list_of_ext in cur_ext:
            ext_check.append(list_of_ext[0])
    
    for d,dirs,files in os.walk(input_path):
        unique = 0
        if cur_dir != dirs:
            cur_dir = dirs
            wcd = True
        else:
            wcd = False
        for f in files:
            unique += 1
            tot_path = os.path.join(d,f)
            try:
                current_ext = f.split('.')[-1]
            except: current_ext = 'binary'
            
            if current_ext not in ext_check:
                ext_check.append(current_ext)
                cur_ext_data.append(current_ext)
                cur_ext_data.append(1)
                cur_ext_data.append(os.path.getsize(tot_path))
                cur_ext_data.append(c_lines(tot_path))
                cur_ext.append(cur_ext_data)
                cur_ext_data = []
            else:
                for cur_list in cur_ext:
                    if (current_ext == cur_list[0]):
                        cur_list[1] = str(int(cur_list[1]) + 1)
                        cur_list[2] = str(int(cur_list[2]) + os.path.getsize(tot_path))
                        cur_list[3] = str(int(cur_list[3]) + c_lines(tot_path))
                        
            if crc_type == 'md5':
               wcd, data = write_file(d,f,otput_path,getsum(tot_path,'md5'),out_file_type,'MD5', wcd, data)
            elif crc_type == 'sha256':
               wcd, data = write_file(d,f,otput_path,getsum(tot_path,'sha256'),out_file_type,'SHA256', wcd, data)
            elif crc_type == 'sha1':
               wcd, data = write_file(d,f,otput_path,getsum(tot_path,'sha1'),out_file_type,'SHA1', wcd, data)
            elif crc_type == 'gost34_94':
                wcd, data = write_file(d,f,otput_path,gost34_11_94(tot_path),out_file_type,'GOST_34_94', wcd, data)
            elif crc_type == 'streebog2':
                wcd, data = write_file(d,f,otput_path,streebog(tot_path,2),out_file_type,'STREEBOG_2', wcd, data)
            elif crc_type == 'streebog5':
                wcd, data = write_file(d,f,otput_path,streebog(tot_path,0),out_file_type,'STREEBOG_5', wcd, data)
            else:
                print('Error HAAAAAAAASH!!!!!! WHERE A U????????')
                sys.exit(1)
    if str(out_file_type) == 'html':
        #if unique > 0:
        data = total_count(input_path,otput_path, data)
        ext_table(otput_path, cur_ext)
        save_conf(otput_path, data, cur_ext, cur_files)
