import os
import codecs
import sqlite3


def paste_Java(id, name, path):
# приводим маркер к бинарному виду из ascii-строки
    marker = bytes(
            ("final class BEEFDEAD0x%04x{}\n" % id)
             .encode('ascii'))

###### [DEBUG] ###
###         print('[D]',
###          'ID: %i' % id, 
###          'Name: %s' % name,
###          'Path: %s' % path)
###### [DEBUG] ###

# пробуем открыть файл
    try: file = open(os.path.join(path, name), "br+")
    except: return False

# если удалось, прочитываем его в буфера
    binBody = file.read()
# вставляем маркер-класс в конец файла
    file.seek(0, os.SEEK_END)
    file.write(marker)
    file.close()
    return True


def paste_Sharp(id, name, path):
    # приводим маркер к бинарному виду из ascii-строки
    marker = bytes(
            ("\n\n#region MyClass definition\n"\
             "  public class tmp%06d {\n"\
             "    static void tmp%08d() {\n"\
             "      ulong[] tmp%08d ={0x%04xBEEFDEAD};\n"\
             "    }\n"\
             "  }\n"\
             "#endregion\n" % (id, id, id,id))
             .encode('ascii'))

###### [DEBUG] ###
###    print('[D]',
###          'ID: %i' % id, 
###          'Name: %s' % name,
###          'Path: %s' % path)
###### [DEBUG] ###

    # пробуем открыть файл
    try: file = open(os.path.join(path, name), "br+")
    except: return False

    # если удалось, прочитываем его в буфера
    binBody = file.read()
    # ищем директиву 'using System'
    #if b"using System;" in binBody:
  #    # если нашли, то просто дописываем в конец файла маркер
  #    file.seek(0, os.SEEK_END)
  #    file.write(marker)
  # else:
  #  # если не нашли, то в начало файла дописываем директиву
    # потом дописываем содержимое файла
    # и вствляем маркер в конец файла
    file.seek(0, os.SEEK_SET)
    file.write(b"using System;")
    file.write(binBody)
    file.write(marker)
    # закрываем файл
    file.close()
    return True


def paste_C(id, name, path):
    #return True
    marker = "\n\n#ifndef NUM_MARKER%08d\n"\
             "#define NUM_MARKER%08d\n"\
             "    static int tmp_%i[]={0xDEADBABA,0x%08x};\n"\
             "#endif\n" % (id, id, id, id)

###### [DEBUG] ###
###    print('[D]',
###          'ID: %i' % id, 
###          'Name: %s' % name,
###          'Path: %s' % path)
###### [DEBUG] ###

    # пробуем открыть файл
    try: file = open(os.path.join(path, name), "r+")
    except: return False
    # если удалось, то вставляем маркер в конец файла
    file.seek(0, os.SEEK_END)
    file.write(marker)
    file.close()
    return True
"""
def paste_Borland(id, name, path):
    #return True
    marker = "\n\n#ifndef NUM_MARKER%04d\n"\
             "#define NUM_MARKER%04d\n"\
             "    static int tmp_%i[]={0xBABA,0x%04x};\n"\
             "#endif\n" % (id, id, id, id)

###### [DEBUG] ###
###    print('[D]',
###          'ID: %i' % id, 
###          'Name: %s' % name,
###          'Path: %s' % path)
###### [DEBUG] ###

    # пробуем открыть файл
    try: file = open(os.path.join(path, name), "r+")
    except: return False
    # если удалось, то вставляем маркер в конец файла
    file.seek(0, os.SEEK_END)
    file.write(marker)
    file.close()
    return True"""

def paste_Python(id, name, path):
    num = '%08x'%(id)
    marker = '\n\nmarker = b\'\\xDE\\xAD\\xBA\\xBA'\
             '\\x%s\\x%s\\x%s\\x%s\'\n'%(num[0:2], num[2:4], num[4:6], num[6:8])
    # пробуем открыть файл
    try: file = open(os.path.join(path, name), "r+")
    except: return False
    # если удалось, то вставляем маркер в конец файла
    file.seek(0, os.SEEK_END)
    file.write(marker)
    file.close()
    return True

def paste_ASM(id, name, path):
    num = '%08x'%(id)
    marker = '\n_marker\tdd\t0DEADBABAh\n_identifier\tdd\t%s%s%s%sh\n'%(num[0:2], num[2:4], num[4:6], num[6:8])
    _DATA = '\n_DATA segment public use16 \'DATA\'%s_DATA\tends\n'

    # пробуем открыть файл
    try: file = open(os.path.join(path, name), "r+", encoding="cp866")
    except: return False
    
    code = file.read()
    offset = code.rfind("_DATA ") - 1
    if offset != -2:
        code = code[:offset] + marker + code[offset:]
    else:
        code += _DATA%marker

    file.seek(0)
    file.write(code)
    file.close()
    return True

def insert(name_pr, path_pr):
# открываем базу данных
    try: con = sqlite3.connect(os.path.join(path_pr, 'Databases', name_pr + '.sdb'))
    except sqlite3.DatabaseError as err:
        return False, 0
    cur = con.cursor()

# получаем список файлов и их пути
    cur.execute("SELECT id_src, name, ext, path FROM Src;")

# в files хранятся кортежи из id, name, ext, path
    files_data = cur.fetchall()

# в цикле обрабатываем каждый файл
    extCpp=['.h', '.cpp', '.H', '.CPP', '.C', '.c']
    extSharp=['.cs', '.CS']
    extJava=['.java', '.JAVA']
    extPython = ['.py', '.PY']
    extAssebly = ['.asm', '.ASM']
    insert_mark = 0
    for id, name, ext, path in files_data:
        if ext in extCpp:
            if paste_C(id, name+ext, path):
                try: cur.execute("UPDATE Src SET is_marked='1' WHERE id_src=%i;" % id)
                except sqlite3.DatabaseError as err: 
                    print('[WARNING]', '-> Обновление табилцы Src <-', err, '[WARNING]', sep = '\n')
                else:
                    con.commit()
                    insert_mark += 1
            else:
                print('[Unknown Error]', '%i\t%s' % (id, name+ext), sep = '\n')
        if ext in extJava:
            if paste_Java(id, name+ext, path):
                try: cur.execute("UPDATE Src SET is_marked='1' WHERE id_src=%i;" % id)
                except sqlite3.DatabaseError as err: 
                    print('[WARNING]', '-> Обновление табилцы Src <-', err, '[WARNING]', sep = '\n')
                else:
                    con.commit()
                    insert_mark += 1
            else:
                print('[Unknown Error]', '%i\t%s' % (id, name+ext), sep = '\n')        
        if ext in extPython:
            if paste_Python(id, name+ext, path):
                try: cur.execute("UPDATE Src SET is_marked='1' WHERE id_src=%i;" % id)
                except sqlite3.DatabaseError as err: 
                    print('[WARNING]', '-> Обновление табилцы Src <-', err, '[WARNING]', sep = '\n')
                else:
                    con.commit()
                    insert_mark += 1
            else:
                print('[Unknown Error]', '%i\t%s' % (id, name+ext), sep = '\n')
        if ext in extAssebly:
            if paste_ASM(id, name+ext, path):
                try: cur.execute("UPDATE Src SET is_marked='1' WHERE id_src=%i;" % id)
                except sqlite3.DatabaseError as err: 
                    print('[WARNING]', '-> Обновление табилцы Src <-', err, '[WARNING]', sep = '\n')
                else:
                    con.commit()
                    insert_mark += 1
            else:
                print('[Unknown Error]', '%i\t%s' % (id, name+ext), sep = '\n')        
        if ext in extSharp:
            if paste_Sharp(id, name+ext, path):
                try: cur.execute("UPDATE Src SET is_marked='1' WHERE id_src=%i;" % id)
                except sqlite3.DatabaseError as err: 
                    print('[WARNING]', '-> Обновление табилцы Src <-', err, '[WARNING]', sep = '\n')
                else:
                    con.commit()
                    insert_mark += 1
            else:
                print('[Unknown Error]', '%i\t%s' % (id, name+ext), sep = '\n')
        else:
            print('[WARNING]', '-> Не найден такой тип файла <-', '[WARNING]')

# закрываем все соединения
    cur.close()
    con.close()
    return True, insert_mark
