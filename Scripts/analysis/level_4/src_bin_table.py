#coding = 'utf-8'
#!/usr/bin/python
import os
import sqlite3
import time
import threading
import subprocess
from . import fix

def create_table(name_pr, path_pr):
# устанавливаем соединение с базой данных
    try: con = sqlite3.connect(os.path.join(path_pr, 'Databases', name_pr + ".sdb"))
    except sqlite3.DatabaseError as err:
        print('[ERROR]','-> Соединение с БД <-', err, '[ERROR]', sep = '\n')
        return
    cur = con.cursor()

# создаем таблицы Src, Bin и Src_Bin_Link в БД
    query_create =  "DROP TABLE IF EXISTS Src;"\
                    "DROP TABLE IF EXISTS Bin;"\
                    "DROP TABLE IF EXISTS Bin_Src_Link;"\
                    "CREATE TABLE Src(id_src INTEGER PRIMARY KEY, name TEXT, ext TEXT,"\
                    "path TEXT, size_byte INTEGER, count_line INTEGER, cs TEXT, is_useful BOOL, "\
                    "is_marked BOOL);"\
                    "CREATE TABLE Bin(id_bin INTEGER PRIMARY KEY, name TEXT, ext TEXT,"\
                    "path TEXT, size_byte INTEGER, cs TEXT);"\
                    "CREATE TABLE Bin_Src_Link(id_bin INTEGER, id_src INTEGER)"
    cur.executescript(query_create)

#генерация рабочих путей
def createPathes(name):
    cur_path = os.getcwd()
    os.chdir('../')
    smile = os.getcwd()
    pathToLab = os.path.join(smile, 'Projects', name, 'Lab')
    pathToDb = os.path.join(smile, 'Projects', name, 'Databases')
    #path_to_crc = os.path.join(smile, 'Resources', 'crc','intcrc.exe')
    path_to_freearc = os.path.join(smile, 'Resources', 'freearc')
    os.chdir(cur_path)
    return pathToLab, pathToDb, path_to_freearc

#снятие КС
def crc(pathToLab, pr_path):
    crc_name = 'md5'
    while True:
        print('Выберите тип контрольной суммы (введите ее номер):','1. MD5','2. SHA256','3. SHA1','4. GOST34_94(в разработке)','5. STREEBOG_2(в разработке)','6. STREEBOG_5(в разработке)',sep = '\n')
        try: choise = int(input('>> '))
        except ValueError: continue

        if choise == 1:    break
        elif choise == 2:
            crc_name = 'sha256'
            break
        elif choise == 3:
            crc_name = 'sha1'
            break
        elif choise == 4:
            crc_name = 'gost34_94'
            break
        elif choise == 5:
            crc_name = 'streebog2'
            break
        elif choise == 6:
            crc_name = 'streebog5'
            break
    fix.check_cs_start(pathToLab, pr_path, 'txt', crc_name)

#парсинг результатов снятия КС
def parseCrc(pr_path, langs_list):
    src = []
    bin = []
    #webTypes = []
    srcTypes = []#, '.sh', '.SH']
    binTypes = []
    c_or_cs = False
    webTypes = ['.js','.JS','.php','.PHP']
    javaTypes_src = ['.java', '.JAVA']
    javaTypes_bin = ['.jar','.JAR','.ear','.EAR','.rpm','.RPM','.war', '.WAR', '.ejb', '.EJB']
    csTypes_src = ['.cs','.CS']
    cppTypes_src = ['.c','.h','.cpp','.hpp','hxx','cxx','.C','.H','.HPP','HXX','CXX','.CPP']
    cs_cppTypes_bin = ['.exe', '.dll','.EXE', '.DLL','.so', '.SO','.o', '.O', '.a', '.A', '.deb','.DEB','.rpm','.RPM', '.bin','.BIN']
    pascalTypes_src = ['.pas', '.PAS']
    python_src = ['.py', '.PY']
    python_bin = ['.pyc', '.PYC', '.pyo', '.PYO']
    perl_src = ['.pl', '.pm', '.cgi', '.PL', '.PM', '.CGI']
    asm_src = ['.asm', '.ASM']
    asm_bin = ['.obj', '.OBJ', '.exe', '.EXE']
    for lang in langs_list:
        if 'Java' in lang:
            srcTypes += javaTypes_src
            binTypes += javaTypes_bin
        elif 'C++' in lang:
            binTypes += cs_cppTypes_bin
            srcTypes += cppTypes_src
        elif 'C#' in lang:
            binTypes += cs_cppTypes_bin
            srcTypes += csTypes_src
        elif 'Web' in lang:
            srcTypes += webTypes
            binTypes += webTypes
        elif 'Pascal' in lang:
            srcTypes += pascalTypes_src
        elif 'Python' in lang:
            binTypes += python_bin
            srcTypes += python_src
        elif 'ASM' in lang:
            binTypes += asm_bin
            srcTypes += asm_src
        elif 'Perl' in lang:
            srcTypes += perl_src

    print(langs_list)
    print(srcTypes)
    print(binTypes)
    input("press any key")

    agenTypes = ['.g', '.g.i', 'TemporaryGeneratedFile_'] #'.xaml',
    crctxt = open(os.path.join(pr_path, 'crc.txt'), 'r', encoding = 'utf-8')
    text = crctxt.readlines()
    for i in range(0,len(text)):
        parted_ln = text[i].split(';')
        cs = parted_ln.pop()
        length = parted_ln.pop()
        full_path = parted_ln.pop()
        basename, ext = os.path.splitext(os.path.basename(full_path))
        if ((agenTypes[0] in basename) or ( agenTypes[1] in basename ) or (agenTypes[2] in basename)):
            continue
        path = os.path.dirname(full_path)
        if ext in srcTypes:
            file = open(full_path, 'rb')
            code = file.readlines()
            file.close()
            count_line = len(code)
            src.append([basename, ext, path, length, count_line, cs, 0, 0])
        elif ext in binTypes:
            bin.append([basename, ext, path, length, cs])
        #if bin not have ext
        elif (ext not in binTypes) & ("C++" in  langs_list):
            f = open(os.path.join(path, basename + ext), 'br')
            header = f.read()
            if len(header) > 0:
                if header[0] == 0x00\
                and header[1] == 0x00\
                and header[2] == 0x38\
                and header[3] == 0x00:
                    bin.append([basename, ext, path, length, cs])
                elif header[0] == 0x7F\
                and header[1] == 0x45\
                and header[2] == 0x4C\
                and header[3] == 0x46:
                    bin.append([basename, ext, path, length, cs])
            f.close()
        crctxt.close()
    return src, bin

#запись кортежей src и bin в БД
def writeSQL(src, bin, path, name):
    con = sqlite3.connect(os.path.join(path, name+'.sdb'))
    cur = con.cursor()
    for i in range(len(src)):
        sql = '%s, \'%s\', \'%s\', \'%s\', %s, %s, \'%s\', %i, %i' % (str(i+1), src[i][0], src[i][1], src[i][2], src[i][3], src[i][4], str(src[i][5]), src[i][6], src[i][7])
        cur.execute('INSERT INTO Src(id_src, name, ext, path, size_byte, count_line, cs, is_useful, is_marked) VALUES('+sql+');')
        if (src[i][6] == 1):
            cur.execute('INSERT INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);' % (i+1, i+1))
    con.commit()
    for i in range(len(bin)):
        sql = '%s, \'%s\', \'%s\', \'%s\', %s, \'%s\''%(str(i+1), bin[i][0], bin[i][1], bin[i][2], str(bin[i][3]), str(bin[i][4]))
        cur.execute('INSERT INTO Bin(id_bin, name, ext, path, size_byte, cs) VALUES('+sql+');')
    con.commit()
    con.close()

#точка входа
def write_table(name, pr_path, langs):
    pathToLab, pathToDb, path_to_freearc = createPathes(name)
    create_table(name, pr_path)
    crc(pathToLab,pr_path)
    src, bin = parseCrc(pr_path, langs)
    len_src = len(src)
    len_bin = len(bin)
    writeSQL(src, bin, pathToDb, name)
    return len_src,len_bin,path_to_freearc