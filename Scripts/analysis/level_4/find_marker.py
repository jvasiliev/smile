import os
import sqlite3
import subprocess
import shutil
from bitstring import BitArray, BitStream

# функция поиска сигнатуры в бинарниках для исходников на С/С++
# сигнатура 0xDEADBABA, id бинарника следует сразу за сигнатурой
# nameList = [(root, name, ext)]
def find_C(id_bin, name, path, con, nameList):
    print('============= Find_C invoked')
    mark = 0
    if len(nameList) != 0:
        for binary in nameList:
            if binary[2] in ['.class', '.CLASS']:
                continue
            mark += find_C(id_bin, binary[1]+binary[2], binary[0], con, [])
        print('%i' % mark)
        return mark
    # if (len(nameList) != 0):
    #     for innerfile in nameList:
    #         inner_str = innerfile.rsplit('\\',1)
    #         if len(inner_str) > 1:
    #             #print(len(inner_str))
    #             mark += find_C(id_bin, inner_str[1], inner_str[0], con, [])
    #     if (mark > 0):
    #         print('%i' % mark)
    #         return mark
    print('%s... ' % (name), end = '')
    cur = con.cursor()
    
    query_update = 'UPDATE Src SET is_useful = 1 WHERE id_src = %i;'
    query_insert = 'INSERT OR REPLACE INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);'
    
    try: bin = open(os.path.join(path, name), 'rb')
    except: return 0
    binBody = bin.read()
    bin.close()
    
    # сравниваем байты считанные из бинарника с заданной сигнатурой
    # Перевод в BinStream
    binaryStream=BitStream(BitArray(binBody))

    #Находим все вхождения!!!
    offsetList=binaryStream.findall('0xBABAADDE', bytealigned=True)
    pos=[]
    for elem in list(offsetList):

        #Перевод в байты
        pos.append(int(elem/8))

    # сравниваем байты считанные из бинарника с заданной сигнатурой
    # найденые макреры (уникальные!!!)
    for i in pos:
        binaryStream.bytepos=i+4
        p4=binaryStream.read('hex:8')
        p3=binaryStream.read('hex:8')
        p2=binaryStream.read('hex:8')
        p1=binaryStream.read('hex:8')
        id_src = int(p1+p2+p3+p4,16)

        # отмечаем исходник с найденным id полезным
        cur.execute(query_update % id_src)
        con.commit()

        # если связь данного исходника с бинарником уже отмечалась, то не обновляем записи
        cur.execute('SELECT * FROM Bin_Src_Link WHERE id_bin=%i AND id_src=%i;' % (id_bin, id_src))
        if cur.fetchall():
            continue

        # отмечаем в базе связь обрабатываемого бинарника и исходника с найденным id
        cur.execute(query_insert % (id_bin, id_src))
        con.commit()
        mark += 1
    cur.close()
    print('%i' % mark)
    return mark  

# функция поиска сигнатуры в бинарниках для исходников на Borland С++
# сигнатура 0xBABA, id бинарника следует сразу за сигнатурой
# nameList = [(root, name, ext)]
def find_Borland(id_bin, name, path, con, nameList):
    print('============= Find_C invoked')
    mark = 0
    if len(nameList) != 0:
        for binary in nameList:
            if binary[2] in ['.class', '.CLASS']:
                continue
            mark += find_C(id_bin, binary[1]+binary[2], binary[0], con, [])
        print('%i' % mark)
        return mark
    # if (len(nameList) != 0):
    #     for innerfile in nameList:
    #         inner_str = innerfile.rsplit('\\',1)
    #         if len(inner_str) > 1:
    #             #print(len(inner_str))
    #             mark += find_C(id_bin, inner_str[1], inner_str[0], con, [])
    #     if (mark > 0):
    #         print('%i' % mark)
    #         return mark
    print('%s... ' % (name), end = '')
    cur = con.cursor()
    
    query_update = 'UPDATE Src SET is_useful = 1 WHERE id_src = %i;'
    query_insert = 'INSERT OR REPLACE INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);'
    
    try: bin = open(os.path.join(path, name), 'rb')
    except: return 0
    binBody = bin.read()
    bin.close()
    
    # сравниваем байты считанные из бинарника с заданной сигнатурой
    # Перевод в BinStream
    binaryStream=BitStream(BitArray(binBody))

    #Находим все вхождения!!!
    offsetList=binaryStream.findall('0xBABA', bytealigned=True)
    pos=[]
    for elem in list(offsetList):

        #Перевод в байты
        pos.append(int(elem/8))

    # сравниваем байты считанные из бинарника с заданной сигнатурой
    # найденые макреры (уникальные!!!)
    for i in pos:
        binaryStream.bytepos=i+2
        p2=binaryStream.read('hex:8')
        p1=binaryStream.read('hex:8')
        id_src = int(p1+p2,16)

        # отмечаем исходник с найденным id полезным
        cur.execute(query_update % id_src)
        con.commit()

        # если связь данного исходника с бинарником уже отмечалась, то не обновляем записи
        cur.execute('SELECT * FROM Bin_Src_Link WHERE id_bin=%i AND id_src=%i;' % (id_bin, id_src))
        if cur.fetchall():
            continue

        # отмечаем в базе связь обрабатываемого бинарника и исходника с найденным id
        cur.execute(query_insert % (id_bin, id_src))
        con.commit()
        mark += 1
    cur.close()
    print('%i' % mark)
    return mark

#функция считающая количество маркеров в .jar
# def scan_names(list):
    # count = 0
    # for name in list:
        # if 'BEEFDEAD' in name:
            # count += 1
    # return count
    
# функция поиска сигнатуры в бинарниках для исходников на С#
# сигнатура 0хDEADBEEF, id бинарника располагается через 11 (для dll)
# и 13 (для exe) байтов. Полный размер сигнатуры 4 байта. Учитывается при 
# формирование id исходника
def find_Sharp(id_bin, name, path, con):
    log = open('rus_names.txt', 'w')
    log.write('### rus_names ###\n')
    
    print('%s... ' % (name), end = '')
    cur = con.cursor()

    try: bin = open(os.path.join(path, name), 'rb')
    except: return 0
    binBody = bin.read()
    bin.close()

    query_update = 'UPDATE Src SET is_useful = 1 WHERE id_src = %i;'
    query_insert = 'INSERT OR REPLACE INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);'

    # Перевод в BinStream
    binaryStream=BitStream(BitArray(binBody))


    # Находим все вхождения!!!
    offsetList=binaryStream.findall('0xADDEEFBE', bytealigned=True)
    pos=[]
    for elem in list(offsetList):

        #Перевод в байты
        pos.append(int(elem/8))

    # сравниваем байты считанные из бинарника с заданной сигнатурой
    mark = 0

    for i in pos:
        binaryStream.bytepos=i+4
        p4=binaryStream.read('hex:8')
        p3=binaryStream.read('hex:8')
        id_src = int(p3+p4,16)
        print('exe %d' % id_src)

        # отмечаем исходники с найденным id полезным
        cur.execute(query_update % id_src)
        con.commit()

        # если связь данного исходника с бинарником уже отмечалась, то не обновляем записи
        cur.execute('SELECT * FROM Bin_Src_Link WHERE id_bin=%i AND id_src=%i;' % (id_bin, id_src))
        if cur.fetchall():
            continue

        # отмечаем в базе связь обрабатываемого бинарника и исходника с найденным id
        cur.execute(query_insert % (id_bin, id_src))
        con.commit()
        mark += 1

    cur.close()
    print('%i' % mark)
    return mark

#функция поиска сигнатуры в директории с .class файлами для исходников на Java
# сигнатура 0xBEEFDEAD, id файла располагается в названии .class файла
#и учитывается при формировании id исходника
# nameList = [(root, name, ext)]
def find_Java(id_bin, name, path, con, nameList):
    print('%s... ' % (name), end = '')
    cur = con.cursor()
    #col = scan_names(nameList)
    
    query_update = 'UPDATE Src SET is_useful = 1 WHERE id_src = %i;'
    query_insert = 'INSERT OR REPLACE INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);'
    
    # сравниваем байты считанные из имени с заданной сигнатурой и устанавливаем id
    mark = 0

    for binary in nameList:
        classname = binary[1]
        if 'BEEFDEAD' in classname:
            id_src = int(classname[8:], 16)
            print('jar %d' % id_bin)
            # отмечаем исходники с найденным id полезным
            cur.execute(query_update % id_src)
            # отмечаем в базе связь обрабатываемого .class файла с найденным id_исходника
            cur.execute(query_insert % (id_bin, id_src))
            mark += 1           

    # for i in range(col):
    #     for j in nameList:
    #         if 'BEEFDEAD' in j:
    #             tmp_sp = os.path.splitext(j)
    #             id_src = int(tmp_sp[0][8:],16)
    #             print('jar %d' % id_bin)
    #             # отмечаем исходники с найденным id полезным
    #             cur.execute(query_update % id_src)
    #             # отмечаем в базе связь обрабатываемого .class файла с найденным id_исходника
    #             cur.execute(query_insert % (id_bin, id_src))
    #             mark += 1
    #             nameList.remove(j)
    #         else:
    #             if (len(nameList) != 0):
    #                 nameList.remove(j)
    #             else:
    #                 return mark
    cur.close()   
    con.commit()
    return mark

def find_Python(id_bin, name, path, con):
    cur = con.cursor()

    try: bin = open(os.path.join(path, name), 'rb')
    except: return 0
    binBody = bin.read()
    bin.close()

    query_update = 'UPDATE Src SET is_useful = 1 WHERE id_src = %i;'
    query_insert = 'INSERT OR REPLACE INTO Bin_Src_Link (id_bin, id_src) VALUES (%i, %i);'

    # Перевод в BinStream
    binaryStream=BitStream(BitArray(binBody))

    # Находим все вхождения!!!
    offsetList=binaryStream.findall('0xDEADBABA', bytealigned=True)
    pos=[]
    for elem in list(offsetList):
        #Перевод в байты
        pos.append(int(elem/8))

        # сравниваем байты считанные из бинарника с заданной сигнатурой
    mark = 0

    for i in pos:
        binaryStream.bytepos = i + 4
        p4=binaryStream.read('hex:8')
        p3=binaryStream.read('hex:8')
        p2=binaryStream.read('hex:8')
        p1=binaryStream.read('hex:8')
        id_src = int(p4+p3+p2+p1, 16)
        print('exe %d' % id_src)

        # отмечаем исходники с найденным id полезным
        cur.execute(query_update % id_src)
        con.commit()

        # если связь данного исходника с бинарником уже отмечалась, то не обновляем записи
        cur.execute('SELECT * FROM Bin_Src_Link WHERE id_bin=%i AND id_src=%i;' % (id_bin, id_src))
        if cur.fetchall():
            continue

        # отмечаем в базе связь обрабатываемого бинарника и исходника с найденным id
        cur.execute(query_insert % (id_bin, id_src))
        con.commit()
        mark += 1

    cur.close()
    print('%i' % mark)
    return mark

# У нас было два пакета травы
# Здесь должен быть скрипт, который, итерируя по ИФ в sdb,
# может обнаружить архив (пакеты rpm, deb) и успешно его
# распаковать, после чего список найденных бинарных файлов подаст
# на опознание соответствующим функциям (find_C, find_Java),
# которые уже и будут разбираться с этими непотребствами.
# Если файл релевантный, то в качестве аргумента подается
# пустой список или NoneType какой-нибудь, который укажет
# функции find_C и ей подобным не теребить массив, а заниматься
# своими прямыми обязанностями.

godforsaken_java_archives = ['.jar', '.ear', '.ejb', '.war', '.JAR', '.EAR', '.EJB', '.WAR']

# Функции unpack_arch, unpack_java и scan_files отвечают
# за формирование nList - списка бинарников, которые
# могут лежать в архивах-исполняемых файлах. Этот список
# подается функциям find_C, find_Java.

# Распаковка первичных пакетов
def unpack_arch(path_pr, name, path, path_to_freearc, ex):
    nList = [] 
    orig_path = os.getcwd()
    if 'Lab' not in orig_path:
        os.chdir(os.path.join(path_pr, 'Lab'))
        if not os.path.isdir(os.path.join(path_pr, 'Lab', 'arch_unpacked')):
            os.mkdir('arch_unpacked')
        os.chdir('arch_unpacked')    
    if not os.path.isdir(os.path.join(os.getcwd(), 'tmp_rpms')):
        os.mkdir('tmp_rpms')
    os.chdir('tmp_rpms')
    if ex in ['.rpm','.RPM']:
        try:
            print('Extracting RPM...')
            subprocess.check_call(os.path.join(path_to_freearc, 'arc.exe') + ' e \"%s\" -di-hanwrftske' % os.path.join(path, name+'.rpm'))
            print('Extracting CPIO...')
            subprocess.check_call(os.path.join(path_to_freearc, 'arc.exe') + ' e \"%s\" -di-hanwrftske' % os.path.join(name + '.cpio'))
        except: pass    
    elif ex in ['.deb', '.DEB']:
        try:
            subprocess.check_call(os.path.join(path_to_freearc, 'arc.exe') + ' e \"%s\" -di-hanwrdftske' % os.path.join(path, name + '.deb'))
            print('Extracting DEB...')
            subprocess.check_call(os.path.join(path_to_freearc, 'arc.exe') + ' e \"%s\" -di-hanwrdftske' % os.path.join('data.tar'))
            print('Extracting TAR...')
        except: pass
    nList += scan_files(os.getcwd(), path_pr)
    os.chdir(orig_path)
    return nList

# Осмотр полученных файлов :3
def scan_files(path, path_pr):
    print('Scanning files in ' + path)
    nList = []
    for root, dirs, files in os.walk(path):
        for file in files:
            name, ext = os.path.splitext(file)
            if ext in godforsaken_java_archives:
                nList += unpack_Java(path_pr, name, ext, root)
                continue
            elif ext == '.class':
                nList.append((root, name, ext))
                continue
            elif ext in ['.deb', '.DEB', '.rpm', '.RPM']:
                nList += unpack_arch(path_pr, name, ext, root)
            print(root)
            print(file)
            print(os.path.join(root, name))
            f = open(os.path.join(root, file), 'br')
            header = f.read(4)
            if len(header) > 0:
                if header[0] == 0x7F\
                and header[1] == 0x45\
                and header[2] == 0x4C\
                and header[3] == 0x46\
                or header[0] == 0x00\
                and header[1] == 0x00\
                and header[2] == 0x38\
                and header[3] == 0x00:
                    nList.append((root, name, ext))
            f.close()
    return nList

# Распаковка Java-архивов. Если ты самый умный, интегрируй её с unpack_arch.
def unpack_Java(path_pr, name, ext, path):
    print('Unpacking ' + path+name+'\\---\\'+ext)
    nList = []
    orig_path = os.getcwd()
    if 'Lab' not in orig_path:
        os.chdir(os.path.join(path_pr, 'Lab'))
        if not os.path.isdir('java_unpacked'):
            os.mkdir('java_unpacked')
        os.chdir('java_unpacked')
    if not os.path.isdir(os.path.join(os.getcwd(), 'tmp')):
        os.mkdir('tmp')
    os.chdir('tmp')
    try:
        subprocess.check_call('jar -xf \"%s\"' % os.path.join(path, name+ext))
        print(os.getcwd())
        nList += scan_files(os.getcwd(), path_pr)
    except: 
        print("Cant extract jar.... going next one")

    os.chdir(orig_path)
    return nList

#Рекурсивная функция удаления (нет багов rmtree) - не работает(((
#def rm_all(path):
    #for root, dirs, files in os.walk(path):
        # for file in files:
            # os.remove(os.path.join(root, file))
        # for dir in dirs:
            # os.rmdir(os.path.join(root, dir))
# Обработчик ошибок rmtree
# def error_handler(func, path, execinfo):
        # e = execinfo[1]
        # if e.errno == errno.ENOENT or not os.path.exists(path):
            # return              # path does not exist - treat as success
        # if func in (os.rmdir, os.remove) and e.errno == errno.EACCES:
            # os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
            # func(path)          # read-only file; make writable and retry
        # else:
            # raise e
    
def find(name_pr, path_pr, path_to_freearc):
    print('Поиск маркеров...')
# открытие БД
    try: con = sqlite3.connect(os.path.join(path_pr, 'Databases', name_pr + '.sdb'))
    except sqlite3.DatabaseError as err:
        return False, 0

# получаем список бинарников и пути к ним
    cur = con.cursor()
    cur.execute('SELECT id_bin, name, ext, path FROM Bin;')

# сохранение результат запроса в files_data
    files_data = cur.fetchall()
    cur.close()
    
# ищем идентификатор в каждом файле
    find_mark = 0
    nList = []
    print(path_pr)
    java_ext = ['.jar', '.JAR', '.ear', '.EAR', '.war', '.WAR', '.ejb', '.EJB']
    linux_ext = ['.rpm','.deb','.RPM','.DEB']
    web_ext = ['.php','.PHP','.js','.JS']
    python_ext = ['.pyc', '.PYC', '.pyo', '.PYO']
    c_ext = ['.exe', '.dll','.EXE', '.DLL','.so', '.SO', '.a', '.A']
    asm_ext = ['.obj', '.OBJ', '.exe', '.EXE']
    orig_path = os.getcwd()    

    for id, name, ext, path in files_data:
        nList = []
        if ext in web_ext:
            find_mark += 1
        elif ext in java_ext:
            nList += unpack_Java(path_pr, name, ext, path)
            find_mark += find_Java(id, name+ext, path, con, nList)
        elif ext in linux_ext:
            nList += unpack_arch(path_pr, name, path, path_to_freearc, ext)
            find_mark += find_C(id, name+ext, path, con, nList)
        elif ext in c_ext:
            find_mark += find_Sharp(id, name+ext, path, con)
            find_mark += find_C(id, name+ext, path, con, [])
            find_mark += find_Borland(id, name+ext, path, con, [])
        elif ext in python_ext:
            find_mark += find_Python(id, name+ext, path, con)
        elif ext in asm_ext:
            find_mark += find_C(id, name+ext, path, con, []) # тк в бинарнике маркер имеет такой же вид как и у сей

        os.chdir(orig_path)
        if os.path.isdir(os.path.join(path_pr, 'Lab', 'arch_unpacked')):
            print('Removing dir')
            #rm_all(os.path.join(path_pr, 'Lab', 'arch_unpacked'))
            shutil.rmtree(os.path.join(path_pr, 'Lab', 'arch_unpacked'), True)
        if os.path.isdir(os.path.join(path_pr, 'Lab', 'java_unpacked')):
            print('Removing dir')
            #rm_all(os.path.join(path_pr, 'Lab', 'java_unpacked'))
            shutil.rmtree(os.path.join(path_pr, 'Lab', 'java_unpacked'), True)
    con.close()
    return True, find_mark