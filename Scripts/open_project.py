from analysis import analysis_project
from analysis.level_4 import fix
from reports import report_generation
from din import din_start

import os
import sqlite3
import shutil
import configparser


def delete_project(path_pr):
    ''' Удаление проекта
    '''
    while True:
        ch = input('Удалить проект [y/n] ?\n>> ')
        if ch == 'y':
            try: shutil.rmtree(path_pr)
            except (shutil.Error, OSError) as err:
                print('[ERROR]', '-> Удаление проекта <-', err, '[ERROR]', sep = '\n') # если файлы заняты другим процессом
                return False
            else:
                print('== Проект удален ==\n')
                return True
        elif ch == 'n':
            return False

def open(name_pr):
    ''' Открывает проект и предоставляет пользователю список действий, которые можно осуществить над проектом
    '''
    print('\n== Работа с проектом ==')

# вводим имя проекта
    while name_pr == '':
        name_pr = input('Для возврата введите \'b\' \nВведите имя проекта: ')
        if name_pr == '':
            print('Указано пустое имя!\n')
            continue 
        if name_pr == 'b':
            return

    print('Текущий проект: ' + name_pr)
# формирование путей
    orign_path = os.getcwd()
    os.chdir('../')
    path_pr = os.path.join(os.getcwd(), 'Projects', name_pr)
    path_rep = os.path.join(os.getcwd(), 'Reports', name_pr)
    path_lab = os.path.join(os.getcwd(), path_pr, 'Lab') # путь к лабораторной версии
    path_orig = os.path.join(os.getcwd(), path_pr, 'Orig') # путь к "чистой" версии
    path_res = os.path.join(os.getcwd(), 'Resources')
    os.chdir(orign_path)

# проверяем существование проекта
    if not os.path.exists(path_pr):
        print('Проекта с таким именем не существует.\n')
        return

# проверяме существование файла config
    if not os.path.exists(os.path.join(path_pr, name_pr + '.conf')):
        print('Конфигурационный файл не найден.')
        return

# предлагаем пользователю выполнить какое-либо д-е над проектом
    while True:
        print ('\n1. Выполнить статический анализ','2. Выполнить динамический анализ','3. Сформировать отчет', '4. Сформировать отчет КС','5. Удалить проект','6. Закрыть проект', sep = '\n')
        try: choise = int(input('-> '))
        except ValueError: continue
    # Статический анализ проекта
        if choise == 1: analysis_project.analysis(name_pr, path_pr, path_lab, path_orig)
    # Динамический анализ проекта
        elif choise == 2: din_start.dynamic_analysis(name_pr, path_pr)
    # формирование отчета
        elif choise == 3: report_generation.general()
    # формирование отчета fix
        elif choise == 4:
            crc_name = 'sha1'
            while True:
                print('Выберите тип контрольной суммы (введите ее номер):','1. MD5','2. SHA256','3. SHA1','4. GOST34_94(в разработке)','5. STREEBOG_2(в разработке)','6. STREEBOG_5(в разработке)',sep = '\n')
                try: choise = int(input('>> '))
                except ValueError: continue

                if choise == 1:
                    crc_name = 'md5' 
                    break
                elif choise == 2:
                    crc_name = 'sha256'
                    break
                elif choise == 3:  break
                elif choise == 4:
                    crc_name = 'gost34_94'
                    break
                elif choise == 5:
                    crc_name = 'streebog2'
                    break
                elif choise == 6:
                    crc_name = 'streebog5'
                    break
            fix.check_cs_start(path_lab, path_pr, 'html', crc_name)
    # удаление проекта
        elif choise == 5:
            if delete_project(path_pr) == True:
                break
        elif choise == 6:
            print('== Проект закрыт ==\n')
            break

