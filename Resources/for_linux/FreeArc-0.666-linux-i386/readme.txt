Welcome to FreeArc 0.666 alpha.

FreeArc is free, open-source archiver for Windows and Linux. It features solid archives with smart updating, AES+Twofish+Serpent encryption, recovery record, self-extracting archives and wide range of compression profiles. It can browse and extract many archive formats including zip, rar 7z, gz, bz2, cab, arj, lzh...

FreeArc works 2-5 times faster than best other compression programs (zip, bzip2, rar, 7-zip) while providing the same compression ratio.

For up-to-date information about FreeArc visit http://freearc.org

===========================================================================

Run "make install" to install Arc, FreeArc, Unarc and support files on your system.
Run "make uninstall" to remove all files installed.
FreeArc GUI requires Gtk+ to work.
FreeArc is 32-bit program.

===========================================================================


Full changelog: http://code.google.com/p/freearc/issues/list?can=1&q=status:fixed%20milestone=0.666
