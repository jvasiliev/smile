﻿******************************************************************************
***  This is a language file for FreeArc (http://freearc.org).             ***
***  You may improve it by providing new translations.                     ***
***  Messages that need translation are marked by "??",                    ***
***  You just need to find all those question signs and replace them with  ***
***  appropriate translations of the text at left side.                    ***
***                                                                        ***
***  Please note that Tooltips section should include not direct           ***
***  translations but TOOLTIPS diplayed at the top of appropriate labels.  ***
***  You may assign tooltip to any label - just use its number +1000 at    ***
***  the left side. For example, label:                                    ***
***    0185 Protection:=Protection: (written in your language, of course)  ***
***  It's tooltip:                                                         ***
***    1185 Protection:=Appends recovery information to archive            ***
******************************************************************************

Metainformation (not translations!)
0000 Language name in english and mother tongue=Kurdish - Sorani (کوردی)
0159 Translated by=Automatic translation from 7-Zip language file.\nPlease edit it to finish translation.\nOriginal copyrights:\nTranslated by Ara Qadir.\nhttp://www.chawg.org
0462 Short language code=ku-ckb

Language-specific helpfiles (not translations!)
Filled only for languages providing their own translations
0256 Helpfile=??
0257 Command-line help=??


=== Explorer context menu =======================================================
0391 Add to "%s"=??
0392 Compress the selected files using FreeArc=??
0393 Add to SFX "%s"=??
0394 Compress the selected files into SFX using FreeArc=??
0395 Add to archive...=??
0396 Compress the selected files using FreeArc via dialog=??
--
0397 Open with FreeArc=??
0398 Open the selected archive(s) with FreeArc=??
0399 Extract to "%s"=??
0400 Extract the selected archive(s) to new folder=??
0401 Extract here=??
0402 Extract the selected archive(s) to the same folder=??
0403 Extract...=??
0404 Extract the selected archive(s) via dialog=??
0405 Test=??
0406 Test the selected archive(s)=??
--
0407 Convert to SFX=??
0408 Convert the selected archive(s) to SFX=??
0409 Convert from SFX=??
0410 Convert the selected SFX(es) to normal archive(s)=??
--
0411 Modify...=??
0412 Modify the selected archives via dialog=??
0413 Join...=??
0414 Join the selected archives via dialog=??
--
0415 Convert to .arc=??
0416 Convert the selected archive(s) to FreeArc format=??
0417 Convert to .arc SFX=??
0418 Convert the selected archive(s) to FreeArc SFX=??
0419 Convert to .arc...=??
0420 Convert the selected archive(s) to FreeArc format via dialog=??


=== Menu ========================================================================
Main menu
0050 File=&فایل
0066 Edit=&دەستکاری
0258 Commands=??
0259 Tools=&ئامڕازەکان
0260 Options=هەڵبژاردنەکان
0261 Help=&یارمەتی

File menu/toolbar
0262 Open archive=کردنەوەی ئەشیڤ
0265 Open archive=??
0263 Select all=هەمووی دیاری بکە
0290 Select all files=??
0037 Select=دیاری بکە
0047 Select files=??
0038 Unselect=دیاری مەکە
0048 Unselect files=??
0264 Invert selection=%پێچەوانەکردنەوەی دیاریکراو
0291 Invert selection=??
0039 Refresh=&بووژاندنەوە
0049 Reread archive/directory=??
0036 Exit=دەر&چوون
0046 Quit application=??

Commands menu/toolbar
0030 Add=زیادکردن
0040 Add files to archive(s)=دیاریکراوەکان بۆ ئەرشیڤ زیاد بکە
0035 Extract=دەرکێشان
0045 Extract files from archive(s)=پەڕگەکان دەرئەکێشێت لە ئەرشیڤە دیاریکراوەکەدا.
0034 Test=تاقیکردنەوە
0044 Test files in archive(s)=گشت ئەرشیڤە دیاریکراوەکە تاقیدەکاتەوە
0086 ArcInfo=زانیاری
0087 Information about archive=??
0033 Delete=&سڕینەوە
0043 Delete files (from archive)=??

Tools menu/toolbar
0266 Lock=??
0267 Lock archive from further changes=??
0268 Comment=لێدوان
0269 Edit archive comment=??
0293 Recompress=??
0294 Recompress files in archive=??
0270 Convert to SFX=??
0271 Convert archive to SFX=??
0426 Convert to FreeArc=??
0427 Convert foreign archive to FreeArc format=??
0272 Encrypt=پاراستن
0273 Encrypt archive contents=??
0274 Protect=??
0275 Add Recovery record to archive=??
0379 Repair=??
0380 Repair damaged archive=??
0031 Modify=??
0041 Modify archive(s)=??
0032 Join=??
0042 Join archives together=??

Options menu/toolbar
0064 Settings=ڕێکخستەکان
0065 Edit program settings=??
0276 View log=??
0277 View logfile=??
0278 Clear log=??
0279 Clear logfile=??

Help menu/toolbar
0280 Main help=??
0281 Help on using FreeArc=??
0282 Cmdline help=??
0283 Help on FreeArc command line=??
0284 Open Homepage=??
0285 Open program site=??
0373 Open forum=??
0374 Open program forum=??
0375 Open wiki=??
0376 Open program wiki=??
0286 Check for update=??
0287 Check for new program versions=??
0288 About=??
0289 About=??


=== File manager ==================================================================
Navibar
0006 Up=??
0007 Save=??
0008 Select files=دیاری بکە...
0009 Unselect files=دیاری مەکە...

Filelist
0015 Name=ناو
0016 Size=قەبارە
0017 Modified=دەستکاریکراوە
0018 DIRECTORY=<بوخچە>

Statusbar
0022 Selected %1 bytes in %2 file(s)=%2 شت دیاریکراوە, %1 بایت
0023 Total %1 bytes in %2 file(s)=%2 شت, %1 بایت

Messages
0012 There are no files selected!=??
0013 There are no archives selected!=??
0071 %1: no such file or directory!=??
0133 You can't compress files directly from archive!=??
0145 You can't join archives directly from archive!=??
0251 Abort operation?=??


=== Dialogs =======================================================================
Archive open dialog
0305 Open archive=??
0306 This file isn't a FreeArc archive!=??
0307 FreeArc archives (*.arc)=??
0308 Archives and SFXes (*.arc;*.exe)=??
0309 All files (*)=??


Add dialog
--Title
0134 Add %1 to archive=دیاریکراوەکان بۆ ئەرشیڤ زیاد بکە
0135 Add %2 files to archive=دیاریکراوەکان بۆ ئەرشیڤ زیاد بکە
0136 Add all files to archive=دیاریکراوەکان بۆ ئەرشیڤ زیاد بکە
0146 Modify all archives=??
0147 Modify %1=??
0148 Modify %2 archives=??
0149 Join all archives=??
0150 Join %1 with another archive=??
0151 Join %2 archives=??
0428 Convert %1 to FreeArc format=??
0429 Convert %2 archives to FreeArc format=??
---------
0182 Main=??
0131 Output archive:=&ئەرشیڤ:
0132 Select output archive=??
0141 Base directory inside archive:=??
--
0188 Store file paths:=??
0189 No=??
0190 Relative to compressed dir=??
0191 Relative to curdir (default)=??
0192 Absolute (relative to root dir)=??
0193 Full (including drive letter)=??
--
0194 Update mode:=&جۆری نوێکاری:
0195 Add and replace files (default)=زیادکردن و جێگرتنەوەی پەڕگەکان
0196 Add and update files=نوێکردنەوە و زیادکردنی پەڕگە
0197 Fresh existing files=بووژاندنەوەی پەڕگە هەبووەکان
0198 Synchronize archive with disk contents=ـکردنی پەڕگەکان Synchronize
--
0183 Compression:=پەستاندن
0184 Encryption:=پاراستن
0185 Protection:=??
0186 Comment:=&لێدوان:
0227 Make EXE:=دروستکردنی ئەرشیڤی SF&X
0128 Test archive after operation=??
0122 Delete files successfully archived=??
0187 Finalize archive=??
---------
0200 Archive=??
0201 Compress each file into separate archive=??
0202 Add to archive name:=??
--
0203 Set archive time to:=??
0204 Current system time=??
0205 Original archive time=??
0206 Latest file time=??
--
0207 Delete previous archive contents=??
0208 Order of files in archive:=??
--
0209 Recompression mode:=??
0210 Quickly append new files=??
0211 Smart recompression of solid blocks (default)=??
0212 Recompress all files=??
0213 Store only fileinfo=??
0214 Store only fileinfo & crcs=??
0215 No archive headers=??
--
0216 Backup mode:=??
0217 No (default)=??
0218 Full: clear \"Archive\" attribute of files succesfully archived=??
0219 Differential: select only files with \"Archive\" attribute set=??
0220 Incremental: select by \"Archive\" attribute & clear it after compression=??
---------
0221 Files=پەڕگەکان
0222 Include only files:=??
0223 Exclude files:=??
0224 Include only files larger than:=??
0225 Include only files smaller than:=??
---------
0199 Comment=لێدوان


Delete dialog
0160 Delete %1 from archive?=دڵنیایت لە سڕینەوەی "%1"؟
0161 Delete %1?=دڵنیایت لە سڕینەوەی "%1"؟
0019 Delete %2 file(s) from archive?=دڵنیایت لە سڕینەوەی ئەم %2 شتە؟
0020 Delete %2 file(s)?=دڵنیایت لە سڕینەوەی ئەم %2 شتە؟
0484 Delete directory %1?=??


Extract dialog
0024 Extract files from %3=دەرکێشان %3
0025 Extract %1 from %3=??
0026 Extract %2 files from %3=??
0027 Extract files from %4 archives=??
0158 Extract all archives=??
0152 Test %3=تاقیکردنەوەی ئەرشیڤ %3
0153 Test %1 from %3=??
0154 Test %2 files from %3=??
0155 Test %4 archives=??
0157 Test all archives=??
--Overwrite mode
0005 Overwrite mode=جۆری بەسەردا نووسینەوە
0001 Ask before overwrite=بپرسە پێش بەسەردا نووسینەوە
0002 Overwrite without prompt=بەسەردا بنووسەوە بەبێ ڕەزامەندی
0003 Update old files=??
0051 Skip existing files=پەڕگە هەبووەکان بپەڕێنە
--Output directory
0004 Output directory:=&دەرکێشان بۆ:
0021 Select output directory=شوێنێک دیاری بکە بۆ پەڕگە دەرکێشراوەکان
0014 Append archive name to the output directory=??
0468 Open output directory in Explorer=??
--Other
0425 Keep broken extracted files=??
0479 Shutdown computer when operation completed=??
0072 Additional options:=??


Archive information dialog
0085 All about %1=??
0465 Archive type:=??
0173 Directories:=بوخچەکان:
0088 Files:=پەڕگەکان:
0089 Total bytes:=سەرجەمی قەبارە:
0090 Compressed bytes:=قەبارەی پەستێنراو:
0091 Ratio:=ڕێژەی پەستاندن:
--
0104 Directory blocks:=??
0463 Directory, bytes:=??
0464 Directory, compressed:=??
0092 Solid blocks:=??
0093 Avg. blocksize:=??
--
0099 Compression memory:=ڕێژەی بەکارهێنانی بیرگە بۆ پەستاندان:
0100 Decompression memory:=ڕێژەی بەکارهێنانی بیرگە بۆ کردنەوەی پەستێنراو:
0105 Dictionary:=قەبارەی &فەرەەنگ:
--
0094 Archive locked:=??
0095 Recovery info:=??
0096 SFX size:=??
0156 Headers encrypted:=&ناوی پەڕگەکان بپارێزە (encrypt)
--
0097 Encryption algorithms:=ڕێبازی پاراستن
0098 Archive comment:=&لێدوان:
---
0449 Solid blocks=??
0450 Position=??
0451 Size=??
0452 Compressed=??
0453 Files=??
0454 Method=??


Settings dialog
0067 Settings=ڕێکخستەکان
0174 Main=??
0068 Language:=زمان:
0069 Edit=&دەستکاری
0070 Import=??
0170 Full name:=??
0171 Copyright:=??
0166 Logfile:=??
0292 View=&بینین
0167 Select logfile=??
0447 Temporary directory:=??
0448 Select directory for temporary files=??
0172 Associate FreeArc with .arc files=FreeArc پەیوەست بکە بە .arc
0471 Associate FreeArc with other archives=??
0370 Watch for new versions via Internet=??
0168 You should restart FreeArc in order for a language settings to take effect.=??
0169 Passwords need to be entered again after restart.=??
--
0466 Interface=??
0361 Add captions to toolbar buttons=??
0469 Show "Test archive" dialog=??
0485 Open .tar.gz-like archives in single step=??
0467 Unpack whole archive when running:=??
--
0421 Explorer integration=??
0422 Enable context menu in Explorer=??
0423 Make it cascaded=??
0424 Enable individual commands:=??
--
0388 Info=??
0461 Largest address space block:=??


Logfiles
0303 No log file specified in Settings dialog!=??
0304 Clear logfile %1?=??

Checking for updates
0295 Checking for updates...=??
0296 Cannot open %1. Do you want to check the page with browser?=??
0297 Nothing new at %1=??
0298 Found new information at %1! Open the page with browser?=??

About dialog
0459 High-performance archiver=??
0460 Free as well for commercial as for non-commercial use=??


=== Profile settings ============================================================
--Compression settings
0106 Compression=پەستاندن
0175 Compression profile:=??
0178 Save=??
0107 Compression level=&ئاستی پەستاندن:
0108 Maximum=زۆرترین
0109 High=??
0110 Normal=ئاسایی
0111 Fast=خێرا
0112 Very fast=خێراترین
0127 HDD-speed=??
0113 Fast, low-memory decompression=??
0226 (fast, low-memory decompression)=??
0176 Filetype auto-detection=??
--Description of compression method selected
0114 Compression level: %1=&ئاستی پەستاندن: %1
0115 Compression speed: %1, memory: %2=ڕێژەی بەکارهێنانی بیرگە بۆ پەستاندان: %2, خێرایی: %1
0116 Decompression speed: %1, memory: %2=ڕێژەی بەکارهێنانی بیرگە بۆ کردنەوەی پەستێنراو: %2, خێرایی: %1
0390 All speeds were measured on 3GHz Core2Duo=??
--Solid block size
0177 Limit solid blocks=??
0138 Bytes, no more than:=قەبارە
0139 Files, no more than:=پەڕگەکان:
0140 Split by extension=??

--Encryption settings
0119 Encryption=پاراستن
0179 Encryption profile:=??
0180 Save=??
0120 Encrypt archive directory=&ناوی پەڕگەکان بپارێزە (encrypt)
0181 Use password=??
0123 Keyfile:=??
0124 Select keyfile=??
0125 Create=??
0126 Create new keyfile=??
0121 Encryption algorithm:=ڕێبازی پاراستن
--Decryption settings
0144 Decryption=??


=== Execution dialogs ============================================================
Many dialogs
0079 &Yes=&بەڵێ
0080 &No=&نەخێر
0362 &OK=باشە
0081 &Cancel=&پاشگەزبوونەوە
0363 &Select=??
0364 &Close=&داخستن
0432 &Detach=??

Progress indicator
--Labels
0056 Files=پەڕگەکان:
0057 Total files=??
0058 Bytes=جێبەجێکراوە:
0059 Total bytes=سەرجەمی قەبارە:
0252 Compressed=قەبارەی پەستێنراو:
0253 Total compressed=??
0060 Ratio=ڕێژەی پەستاندن:
0061 Speed=خێرایی:
0062 Time=ک. دەستپێکردوو:
0063 Total time=??
--Messages
0246 Found %1 files=??
0247 Found %1 archives=??
0248 Analyzed %1 files=??
0249 Reading archive directory=??
0250 Sorting filelist=??
0385 Scanning archive for damages=??
0386 Protecting archive from damages=??
0387 Recovering archive=??
--Extra options
0446 Keep window on top=??
--Buttons
0052 &Background=&پاشەبنەما
0053 &Pause=&ڕاگرتن
0054 &Continue=&بەردەوامبوون
0470 &Close=&داخستن

File overwrite dialog
0078 Confirm File Replace=دڵنیابە لە جێگرتنەوەی پەڕگە
0162 Destination folder already contains processed file.=بوخچەی مەبەست پەڕگەیەکی تیادایە بە هەمان ناو.
0163 Would you like to replace the existing file=ئەتەوێت پەڕگە هەبووەکان جێبگیردرێتەوە؟
0164 with this one?=لەگەڵ ئەم دانەیە؟
0165 %1\n%2 bytes\nmodified on %3=%1\n%2 بایت\nدەستکاریکراوە لە %3
0082 Yes to &All=بەڵێ بۆ &هەموو
0083 No to A&ll=نەخێر بۆ هە&موو
0084 &Update all=??

Enter password dialog
0076 Enter encryption password=نووسینی تێپەڕەوشە:
0077 Enter decryption password=نووسینی تێپەڕەوشە:
0074 Enter password:=تێپەڕەوشە بنووسە:
0075 Reenter password:=تێپەڕەوشە بنووسەوە:

Archive comment dialog
0073 Enter archive comment=??


=== Infoline messages ====================================================
0439 Listing archive %1=??
0445 SUCCESFULLY LISTED %1=??
0442 %2 WARNINGS WHILE LISTING %1=??

0435 Deleting files from archive %1=??
0229 FILES SUCCESFULLY DELETED FROM %1=??
0230 %2 WARNINGS WHILE DELETING FROM %1=??

0440 Testing archive %1=??
0232 SUCCESFULLY TESTED %1=??
0233 %2 WARNINGS WHILE TESTING %1=??

0441 Extracting files from archive %1=??
0235 FILES SUCCESFULLY EXTRACTED FROM %1=??
0236 %2 WARNINGS WHILE EXTRACTING FILES FROM %1=??

0433 Modifying archive %1=??
0238 SUCCESFULLY MODIFIED %1=??
0239 %2 WARNINGS WHILE MODIFYING %1=??

0240 Joining archives to %1=??
0241 SUCCESFULLY JOINED ARCHIVES TO %1=??
0242 %2 WARNINGS WHILE JOINING ARCHIVES TO %1=??

0437 Creating archive %1=??
0443 SUCCESFULLY CREATED %1=??
0434 %2 WARNINGS WHILE CREATING %1=??

0438 Updating archive %1=??
0444 SUCCESFULLY UPDATED %1=??
0436 %2 WARNINGS WHILE UPDATING %1=??

0299 Lock archive(s)?=??
0300 Locking archive %1=??
0301 SUCCESFULLY LOCKED ARCHIVE %1=??
0302 %2 WARNINGS WHILE LOCKING ARCHIVE %1=??

0381 Repair archive(s)? Repaired archive(s) will be placed into files named fixed.*=??
0382 Repairing archive %1=??
0383 SUCCESFULLY REPAIRED ARCHIVE %1=??
0384 %2 WARNINGS WHILE REPAIRING ARCHIVE %1=??


=== Console messages ====================================================
0480 Compressing %1=??
0481 Testing %1=??
0482 Extracting %1=??
0483 Skipping %1=??


=== Error messages =======================================================
0316 ERROR: %1=??
0317 WARNING: %1=??
0310 can't modify archive locked with -k=??
0311 can't create temporary file=??
0312 output archive already exists, keeping temporary file %1=??
0313 archive broken, keeping temporary file %1=??
0314 archive broken, deleting=??
0315 can't open SFX module %1=??
0318 command syntax is "%1"=??
0319 options %1 and %2 can't be used together=??
0320 unknown command "%1". Supported commands are: %2=??
0321 unknown option "%1"=??
0322 ambiguous option "%1" - is that %2?=??
0325 option "%1" have illegal format=??
0326 %1 option must be one of: %2=??
0327 no command name in command: %1=??
0328 no archive name in command: %1=??
0329 no filenames in command: %1=??
0330 can't read directory "%1"=??
0331 can't get info about file "%1"=??
0332 can't open file "%1"=??
0334 bad section %1 in %2=??
0455 Operation terminated by user!=??
0456 Program terminated by user!=??
0337 no files, erasing empty archive=??
0338 skipped %1 fake files=??
0339 bad password for archive %1=??
0340 bad password for %1 in archive %2=??
0341 %1 isn't archive or this archive is corrupt: %2. Please recover it using 'r' command or use -tp- option to ignore Recovery Record=??
0342 SFX module %1 is not found=??
0343 %1 isn't implemented=??
0344 only first of %1 recovery records can be processed by this program version. Please use newer versions to process the rest=??
0345 you need FreeArc %1 or above to process this recovery info=??
0346 file %1 already exists=??
0347 archive can't be recovered - recovery data absent or corrupt=??
0348 %1 unrecoverable errors (%2) found, can't restore anything!=??
0349 can't open original at %1=??
0350 %1 has size %2 so it can't be used to recover %3 having size %4=??
0351 %1 errors (%2) remain unrecovered=??
0352 found %1 errors (%2)=??
0353 there were %1 warnings due archive testing=??
0354 block descriptor at pos %1 is corrupted=??
0355 %1 is corrupted=??
0359 %1 failed decompression=??
0360 %1 should be uncompressed=??
0356 archive directory not found=??
0357 archive signature not found at the end of archive=??
0358 last block of archive is not footer block=??
0377 command "%1" shouldn't have additional arguments=??
0378 bad name or parameters in encryption algorithm %1=??
0472 Unsupported compression method for "%1".=ڕێبازێکی پاڵپشتی نەکراوی پەستاندن بۆ "%1". 
0473 Data error in "%1". File is broken.=زانیاری "%1" هەڵەیە. پەڕگە تێکشکاوە.
0474 Data error in encrypted file "%1". Wrong password?=زانیاری هەڵەیە لە پەڕگەی پارێزراودا "%1". ئایا تێپەڕەوشە هەڵەیە؟
0475 CRC failed in "%1". File is broken.=CRC سەرکەوتوو نەبوو. پەڕگە تێکشکاوە.
0476 CRC failed in encrypted file "%1". Wrong password?=CRC هەڵەیە لە پەڕگەی پارێزراودا "%1". ئایا تێپەڕەوشە هەڵەیە؟
0477 Unknown error=هەڵەیەکی نەزانراو
0478 can't modify non-FreeArc archive=??

Errors returned by compression methods
0365 general (de)compression error in %1=??
0366 invalid compression method or parameters in %1=??
0367 can't allocate memory required for (de)compression in %1=??
0369 bad compressed data in %1=??
0430 read error (bad media?) in compression algorithm %1=??
0431 write error (disk full?) in compression algorithm %1=??


=== Standard profiles ====================================================
Compression
0771 Maximum=??
0772 Maximum with fast decompression=??
0773 Ultra (require 2 gb RAM for decompression)=??
0774 Maximum (require 1 gb RAM for decompression)=??
0775 Best asymmetric (with fast decompression)=??
0752 No compression=??

Encryption
0733 Strongest=??
0732 Strong=??
0730 Standard=??
0731 Fast=??

Protection
0769 Add (autosize, 1-4%)=??
0735 Remove=??
0734 Add=??
0770 Add for recovery via Internet=??

Comment
0740 Add from textbox=??
0741 Remove=??
0742 Add short comment, example=??
0743 Add from a file, example=??

SFX
0765 Windows GUI=??
0766 Windows console=??
0767 Linux console=??
0768 Convert EXE back to ARC=??

Sorting
0744 No sorting=??
0745 Standard=??
0746 Advanced=??
0747 Smart+Path=??
0748 Smart+Name=??
0750 Extension+Path=??
0749 Extension+Name=??
0751 Extension+Size=??

Archive name template
0754 Date+Time=??
0755 Date=??
0756 Time=??
0757 Month=??
0758 Monthday=??
0759 Weekday=??

Exclude
0753 Backup files=??

Additional compression options
0760 Perform full backup=??
0761 Full archive test before and after operation=??

Additional extraction options
0762 Remove pathnames=??
0763 Restore absolute pathnames=??
0764 Full archive test before performing operation=??


=== Tooltips (index=N+1000) ===========================================

******************************************************************************************
***  DON'T FORGET THAT THIS SECTION SHOULD PROVIDE TOOLTIPS, NOT DIRECT TRANSLATIONS.  ***
***  You may find some tooltips in arc.english.txt                                     ***
******************************************************************************************

Add dialog
--
1183 Compression=??
1184 Encryption=??
1185 Protection:=??
1186 Comment=??
1227 Make EXE:=??
1128 Test archive after operation=??
1122 Delete files successfully archived=??
1187 Finalize archive=??
1072 Additional options:=??
--
1201 Compress each marked file/directory into separate archive=??
1202 Add to archive name=??
--
1207 Delete previous archive contents=??
1208 Order of files in archive=??
--
1222 Include only files=??
1223 Exclude files=??

--Settings dialog
1467 Unpack whole archive when running:=??
1469 Show "Test archive" dialog=??

--Compression settings
1113 Fast, low-memory decompression=??
1176 Filetype auto-detection=??
1138 Bytes, no more than:=??
1139 Files, no more than:=??
1140 Split by extension=??

--Encryption settings
1120 Encrypt archive directory=??
1123 Keyfile:=??
1125 Generate new keyfile=??
1181 Use password=??


=== Everything else ====================================================
Common words
0101 Yes=??
0102 No=??
0323 or=??
0324 and=??

Failure messages
0010 Operation interrupted!=??
0011 No threads to run: infinite loop or deadlock?=??
